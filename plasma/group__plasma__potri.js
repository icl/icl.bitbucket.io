var group__plasma__potri =
[
    [ "plasma_cpotri", "group__plasma__potri.html#ga3a49d436d2ef9cb097ea6d2db6ade9ae", null ],
    [ "plasma_omp_cpotri", "group__plasma__potri.html#ga04eca2cc7779a3763b88a37570b0ced1", null ],
    [ "plasma_dpotri", "group__plasma__potri.html#gaea3c02765ef8aad77fa1f68284c43c2e", null ],
    [ "plasma_omp_dpotri", "group__plasma__potri.html#ga36f7e2ab509eca2ac2cc9a52690f7874", null ],
    [ "plasma_spotri", "group__plasma__potri.html#gaac671e7f306aaa6ad4f3c62f47f3e8e9", null ],
    [ "plasma_omp_spotri", "group__plasma__potri.html#ga39ec3b7a106a4fde5132cc781d91deac", null ],
    [ "plasma_zpotri", "group__plasma__potri.html#gac4ce6d02fe4c7aac54023f1003dd99ac", null ],
    [ "plasma_omp_zpotri", "group__plasma__potri.html#ga1e55484facf47cdde0074e49d21b0ae5", null ]
];