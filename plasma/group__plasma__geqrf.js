var group__plasma__geqrf =
[
    [ "plasma_cgeqrf", "group__plasma__geqrf.html#ga3acaf0804650a04af10ff23df52341b2", null ],
    [ "plasma_omp_cgeqrf", "group__plasma__geqrf.html#ga4a4dd0d4ebf3f33bf538dd919d0f46b8", null ],
    [ "plasma_dgeqrf", "group__plasma__geqrf.html#ga2959d20fc5e85ff8e9673e927e208a43", null ],
    [ "plasma_omp_dgeqrf", "group__plasma__geqrf.html#gad9591ddf6d04fbde5863197254ff6cad", null ],
    [ "plasma_sgeqrf", "group__plasma__geqrf.html#ga1d174aeacc31a2829178a9706f277a3a", null ],
    [ "plasma_omp_sgeqrf", "group__plasma__geqrf.html#gab910106d396329cf24e7cca2513ab994", null ],
    [ "plasma_zgeqrf", "group__plasma__geqrf.html#ga782ce015b805aa6172eb75b2bd6e47a5", null ],
    [ "plasma_omp_zgeqrf", "group__plasma__geqrf.html#ga59c38b2a0784528c0f33df5955f147a9", null ]
];