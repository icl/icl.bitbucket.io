var group__plasma__trtri =
[
    [ "plasma_ctrtri", "group__plasma__trtri.html#ga4c0322eecdd97feed11468c0212a5c7d", null ],
    [ "plasma_omp_ctrtri", "group__plasma__trtri.html#gacce3c0a301ad37690cd1f08a1dac82d6", null ],
    [ "plasma_dtrtri", "group__plasma__trtri.html#ga6fc47172f36d5d1ec5d6dc5084ae8e57", null ],
    [ "plasma_omp_dtrtri", "group__plasma__trtri.html#gabd89a9effe12edbc7e5a10c037ef3cb8", null ],
    [ "plasma_strtri", "group__plasma__trtri.html#gacc5ba61d7d33f40a90d46889e1f4843f", null ],
    [ "plasma_omp_strtri", "group__plasma__trtri.html#ga7906cfcbf8a9d52fa1829fe70f0b50d0", null ],
    [ "plasma_ztrtri", "group__plasma__trtri.html#gafa92fc6a2afd0ff2daea328142c97246", null ],
    [ "plasma_omp_ztrtri", "group__plasma__trtri.html#gac1d99c9c732561c0e0a751875576b2aa", null ]
];