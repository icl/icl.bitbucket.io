var group__group__svd =
[
    [ "gesvd: SVD using QR iteration", "group__plasma__gesvd.html", null ],
    [ "gesdd: SVD using divide-and-conquer", "group__plasma__gesdd.html", null ],
    [ "gebrd: Bidiagonal reduction", "group__plasma__gebrd.html", null ],
    [ "or/unmbr: Multiplies by Q or P from bidiagonal reduction", "group__plasma__unmbr.html", null ],
    [ "or/ungbr: Generates     Q or P from bidiagonal reduction", "group__plasma__ungbr.html", null ],
    [ "Auxiliary routines", "group__group__gesvd__aux.html", null ]
];