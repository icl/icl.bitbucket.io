var group__plasma__trsm =
[
    [ "plasma_ctrsm", "group__plasma__trsm.html#ga6e96b60bd0efd69e45eb7408371ef7bc", null ],
    [ "plasma_omp_ctrsm", "group__plasma__trsm.html#ga9ca44f22608d1ca88ea6c4bc5ddcceae", null ],
    [ "plasma_dtrsm", "group__plasma__trsm.html#gaa6ef184409863f8098b8833f87d1a6e8", null ],
    [ "plasma_omp_dtrsm", "group__plasma__trsm.html#gad132c0570f19adc44340e5372446cbd2", null ],
    [ "plasma_strsm", "group__plasma__trsm.html#gac85762f9b8bb8641dad9a7575ed9dbe0", null ],
    [ "plasma_omp_strsm", "group__plasma__trsm.html#gafd8fc4ece2d9a321c9e92ebef0820ff9", null ],
    [ "plasma_ztrsm", "group__plasma__trsm.html#ga9ab02a0dd0a7f60b7c7a6fd6bc1be950", null ],
    [ "plasma_omp_ztrsm", "group__plasma__trsm.html#ga1b61dfed48424ebbf1281b0372325351", null ]
];