var group__core__norms =
[
    [ "lange: General matrix norm", "group__core__lange.html", "group__core__lange" ],
    [ "lanhe: Hermitian matrix norm", "group__core__lanhe.html", null ],
    [ "lansy: Symmetric matrix norm", "group__core__lansy.html", null ],
    [ "lantr: Triangular matrix norm", "group__core__lantr.html", null ]
];