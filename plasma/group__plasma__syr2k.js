var group__plasma__syr2k =
[
    [ "plasma_csyr2k", "group__plasma__syr2k.html#ga4f3aa9ccf057dda195badca6ba6e72f4", null ],
    [ "plasma_omp_csyr2k", "group__plasma__syr2k.html#ga88afe934aa876be6ed4d571aeb727b62", null ],
    [ "plasma_dsyr2k", "group__plasma__syr2k.html#gac1c501349847b1419a3a58b38299d9e0", null ],
    [ "plasma_omp_dsyr2k", "group__plasma__syr2k.html#ga714c774c796c8e2b321417107b6b801e", null ],
    [ "plasma_ssyr2k", "group__plasma__syr2k.html#ga7b14855c0e8253506158e3e24431b7a4", null ],
    [ "plasma_omp_ssyr2k", "group__plasma__syr2k.html#ga358e620ab37d8df729917285bef892fc", null ],
    [ "plasma_zsyr2k", "group__plasma__syr2k.html#ga1de055aa5c419bc79e9ea4c6f19db5f4", null ],
    [ "plasma_omp_zsyr2k", "group__plasma__syr2k.html#gaf408912d8462f8c88016e982bf6629e8", null ]
];