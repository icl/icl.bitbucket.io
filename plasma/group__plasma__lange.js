var group__plasma__lange =
[
    [ "plasma_clange", "group__plasma__lange.html#gab6cc2e8004bf4135e308738de3632c79", null ],
    [ "plasma_omp_clange", "group__plasma__lange.html#ga800ca7a1085eaa5c9fc7192fbccd0a9f", null ],
    [ "plasma_dlange", "group__plasma__lange.html#gab5ab770141d025a44961fc0453eba278", null ],
    [ "plasma_omp_dlange", "group__plasma__lange.html#ga457b09fb3bbfcd3d396fb41748cfbfe0", null ],
    [ "plasma_slange", "group__plasma__lange.html#ga5d84b26b95f41725ed6447fe5d64dc4a", null ],
    [ "plasma_omp_slange", "group__plasma__lange.html#ga13d04e655810ea78ecee486238b832c7", null ],
    [ "plasma_zlange", "group__plasma__lange.html#gaab3dab5a0ed4bc3ef91642b88c963d83", null ],
    [ "plasma_omp_zlange", "group__plasma__lange.html#gad810a80aca11a3a57960b01c0cf9dec8", null ]
];