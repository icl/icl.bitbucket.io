var group__plasma__unmlq =
[
    [ "plasma_cunmlq", "group__plasma__unmlq.html#ga8387de1b34593a495d57a6fc7ae5fdd9", null ],
    [ "plasma_omp_cunmlq", "group__plasma__unmlq.html#gafe3741eb82dd2570e055d583af03d558", null ],
    [ "plasma_dormlq", "group__plasma__unmlq.html#ga42422239b5a3bfe01de3c0be89196a40", null ],
    [ "plasma_omp_dormlq", "group__plasma__unmlq.html#ga9d7079ebc5ceed3b85e3a5ed61922d02", null ],
    [ "plasma_sormlq", "group__plasma__unmlq.html#gae11802403becf3eff050b7ccc69f7522", null ],
    [ "plasma_omp_sormlq", "group__plasma__unmlq.html#ga480faba1f1e87612ec8242f9e959fdd9", null ],
    [ "plasma_zunmlq", "group__plasma__unmlq.html#ga054cc2c65f23069afba9c91183d68d7a", null ],
    [ "plasma_omp_zunmlq", "group__plasma__unmlq.html#gac8c47bc72f71ffa22dbdde36a57b34b1", null ]
];