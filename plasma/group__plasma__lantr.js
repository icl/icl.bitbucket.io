var group__plasma__lantr =
[
    [ "plasma_clantr", "group__plasma__lantr.html#ga9f3fa16fe7c39544dd5d0e856e2230bb", null ],
    [ "plasma_omp_clantr", "group__plasma__lantr.html#ga80f0948d3b08c40c37057ed0bdfa35ba", null ],
    [ "plasma_dlantr", "group__plasma__lantr.html#ga042fb72e6b573c20ef307a6e96298558", null ],
    [ "plasma_omp_dlantr", "group__plasma__lantr.html#ga72c6ee2b03f8c5c5bbcfa7d6690a1237", null ],
    [ "plasma_slantr", "group__plasma__lantr.html#gad4fc9a1a14a0ccb4b1a314bbb0b19aae", null ],
    [ "plasma_omp_slantr", "group__plasma__lantr.html#gac56d7064b70d789d30911b329f3f819c", null ],
    [ "plasma_zlantr", "group__plasma__lantr.html#ga4983c3a423a6a88eb616a546523e29f8", null ],
    [ "plasma_omp_zlantr", "group__plasma__lantr.html#ga35cd10fd63464bbbca7e25c9a1965c94", null ]
];