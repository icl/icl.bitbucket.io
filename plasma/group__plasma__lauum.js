var group__plasma__lauum =
[
    [ "plasma_clauum", "group__plasma__lauum.html#gad979c2efbb703703393feddea0e7193f", null ],
    [ "plasma_omp_clauum", "group__plasma__lauum.html#gabd60585396f55abdd53601245f413b27", null ],
    [ "plasma_dlauum", "group__plasma__lauum.html#ga3480592911b9129a58b81949571d374e", null ],
    [ "plasma_omp_dlauum", "group__plasma__lauum.html#ga301a28aa27e59c70ffa6eaf695226aad", null ],
    [ "plasma_slauum", "group__plasma__lauum.html#ga2066318144ff2145547f6caa8f9a1537", null ],
    [ "plasma_omp_slauum", "group__plasma__lauum.html#gaf5b090be6fc045d4de4aaf62f7a6e140", null ],
    [ "plasma_zlauum", "group__plasma__lauum.html#ga33f83b7a1318732fbefbb647f4893fb0", null ],
    [ "plasma_omp_zlauum", "group__plasma__lauum.html#ga4957f1f6b834e0d5cb35d159e3b7e1b7", null ]
];