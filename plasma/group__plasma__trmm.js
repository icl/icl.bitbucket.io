var group__plasma__trmm =
[
    [ "plasma_ctrmm", "group__plasma__trmm.html#gafa5d0f4e46fd63812e35179231c50fd9", null ],
    [ "plasma_omp_ctrmm", "group__plasma__trmm.html#ga345ac56212a229c0f7dbfcd55adce8c1", null ],
    [ "plasma_dtrmm", "group__plasma__trmm.html#ga8989ec5249b4da6ac3441a67513c8d8b", null ],
    [ "plasma_omp_dtrmm", "group__plasma__trmm.html#gad7f84e7cdfcbcdd966df459b154ef20b", null ],
    [ "plasma_strmm", "group__plasma__trmm.html#gafa742cac3740e38c3811619a85e480f9", null ],
    [ "plasma_omp_strmm", "group__plasma__trmm.html#gaf52941f2beb8961349e49bbfb5ef7ba0", null ],
    [ "plasma_ztrmm", "group__plasma__trmm.html#ga74cec8254d641f0ace0fb1b93b305075", null ],
    [ "plasma_omp_ztrmm", "group__plasma__trmm.html#ga55c0f423533031f5e9222ed798eb5006", null ]
];