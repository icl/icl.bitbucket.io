var group__core__blas2 =
[
    [ "geadd: Add matrices", "group__core__geadd.html", null ],
    [ "gemv:       General matrix-vector multiply", "group__core__gemv.html", null ],
    [ "ger:        General matrix rank 1 update", "group__core__ger.html", null ],
    [ "hemv:    Hermitian matrix-vector multiply", "group__core__hemv.html", null ],
    [ "her:     Hermitian rank 1 update", "group__core__her.html", null ],
    [ "her2:    Hermitian rank 2 update", "group__core__her2.html", null ],
    [ "symv:    Symmetric matrix-vector multiply", "group__core__symv.html", null ],
    [ "syr:     Symmetric rank 1 update", "group__core__syr.html", null ],
    [ "syr2:    Symmetric rank 2 update", "group__core__syr2.html", null ],
    [ "trmv:       Triangular matrix-vector multiply", "group__core__trmv.html", null ],
    [ "trsv:       Triangular matrix-vector solve", "group__core__trsv.html", null ],
    [ "lacpy:  Copy matrix", "group__core__lacpy.html", "group__core__lacpy" ],
    [ "lascl:  Scale matrix by scalar", "group__core__lascl.html", null ],
    [ "lascl2: Scale matrix by diagonal", "group__core__lascl2.html", null ],
    [ "laset:  Set matrix to constants", "group__core__laset.html", "group__core__laset" ]
];