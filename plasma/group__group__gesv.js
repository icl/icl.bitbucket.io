var group__group__gesv =
[
    [ "gesv:  Solves Ax = b using LU factorization (driver)", "group__plasma__gesv.html", "group__plasma__gesv" ],
    [ "getrf: LU factorization", "group__plasma__getrf.html", null ],
    [ "getrs: LU forward and back solves", "group__plasma__getrs.html", null ],
    [ "getri: LU inverse", "group__plasma__getri.html", "group__plasma__getri" ],
    [ "gerfs: Refine solution", "group__plasma__gerfs.html", null ],
    [ "Auxiliary routines", "group__group__gesv__aux.html", "group__group__gesv__aux" ]
];