var group__group__larf =
[
    [ "larf:  Apply Householder reflector to general matrix", "group__plasma__larf.html", null ],
    [ "larfy: Apply Householder reflector to symmetric/Hermitian matrix", "group__plasma__larfy.html", null ],
    [ "larfg: Generate Householder reflector", "group__plasma__larfg.html", null ],
    [ "larfb: Apply block of Householder reflectors (Level 3)", "group__plasma__larfb.html", null ]
];