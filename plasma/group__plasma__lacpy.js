var group__plasma__lacpy =
[
    [ "plasma_clacpy", "group__plasma__lacpy.html#gaf723b469e23f46a279d92cf5baadde14", null ],
    [ "plasma_omp_clacpy", "group__plasma__lacpy.html#gae5d39d4a6de5151923f37e48cd66559a", null ],
    [ "plasma_dlacpy", "group__plasma__lacpy.html#gab1094d657c11967baeb53ec1d12984c5", null ],
    [ "plasma_omp_dlacpy", "group__plasma__lacpy.html#gab08e213adcfb9927da2676f649d5bff0", null ],
    [ "plasma_slacpy", "group__plasma__lacpy.html#ga4348475ed502d3b46bb9bb420b0a7930", null ],
    [ "plasma_omp_slacpy", "group__plasma__lacpy.html#gab5d3f2b44e7c5fc0f0bbfea1cefdc74c", null ],
    [ "plasma_zlacpy", "group__plasma__lacpy.html#gac57523b11167badfee361ea2cffdcf39", null ],
    [ "plasma_omp_zlacpy", "group__plasma__lacpy.html#ga8a0b6a0ecae7c055feb7f6626079377c", null ]
];