var group__plasma__unmqr =
[
    [ "plasma_cunmqr", "group__plasma__unmqr.html#gab7bf40f8715136391df906d445102c57", null ],
    [ "plasma_omp_cunmqr", "group__plasma__unmqr.html#ga5bbc7a8ac23ec20c8064bb8d987e1595", null ],
    [ "plasma_dormqr", "group__plasma__unmqr.html#gae8ca5b9266552acaa253a35783aa2860", null ],
    [ "plasma_omp_dormqr", "group__plasma__unmqr.html#gafb3d29dac30e4871021dcea6c7864985", null ],
    [ "plasma_sormqr", "group__plasma__unmqr.html#gaf2365ffe7704b045cdd591ee2615c26f", null ],
    [ "plasma_omp_sormqr", "group__plasma__unmqr.html#gab5cbdad40b69a2bc8968b221c60c512e", null ],
    [ "plasma_zunmqr", "group__plasma__unmqr.html#gad9e5db74849f1706782f865f6efd1c1c", null ],
    [ "plasma_omp_zunmqr", "group__plasma__unmqr.html#gadd70d1cfd9870c9a7c031c5ba51dc558", null ]
];