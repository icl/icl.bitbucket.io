var group__plasma__const =
[
    [ "plasma_diag_const", "group__plasma__const.html#ga6a4fddad291e460b521f4137be6ba334", null ],
    [ "plasma_direct_const", "group__plasma__const.html#ga57cf050d99ae7af02f1c071efe918475", null ],
    [ "plasma_norm_const", "group__plasma__const.html#gaf93524bc197a7a879acafe65767ebd2a", null ],
    [ "plasma_side_const", "group__plasma__const.html#gae97fa2648110002faaaeee0b214aa266", null ],
    [ "plasma_storev_const", "group__plasma__const.html#gae35bf70e0d2cb2732683108ba4e6aa64", null ],
    [ "plasma_trans_const", "group__plasma__const.html#gad967a6e29b40b36a97b50f845677bfe9", null ],
    [ "plasma_uplo_const", "group__plasma__const.html#gab9f886d59b2e3ca07264a1327901b7a7", null ],
    [ "lapack_const", "group__plasma__const.html#gaccc39aa2cc2d99e727c5677cfbba10f5", null ]
];