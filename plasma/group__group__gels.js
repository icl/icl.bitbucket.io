var group__group__gels =
[
    [ "gels: Least squares solve of Ax = b using QR or LQ factorization (driver)", "group__plasma__gels.html", "group__plasma__gels" ],
    [ "gelqs: Back solve using LQ factorization of A", "group__plasma__gelqs.html", "group__plasma__gelqs" ],
    [ "geqrs: Back solve using QR factorization of A", "group__plasma__geqrs.html", "group__plasma__geqrs" ]
];