var searchData=
[
  ['a12',['A12',['../structplasma__desc__t.html#a5081e56a01179e8b9305912676909a13',1,'plasma_desc_t']]],
  ['a21',['A21',['../structplasma__desc__t.html#a9749348dffa9c3584312c581f4c2bc20',1,'plasma_desc_t']]],
  ['a22',['A22',['../structplasma__desc__t.html#ad8485dcb9895aee64a447c70642cb554',1,'plasma_desc_t']]],
  ['asum_3a_20_20sum_20vector',['asum:  Sum vector',['../group__core__asum.html',1,'']]],
  ['axpy_3a_20_20add_20vectors',['axpy:  Add vectors',['../group__core__axpy.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__geev__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__gesv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__gesvd__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__heev__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__hesv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__posv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__qr__aux.html',1,'']]],
  ['asum_3a_20_20sum_20vector',['asum:  Sum vector',['../group__plasma__asum.html',1,'']]],
  ['axpy_3a_20_20add_20vectors',['axpy:  Add vectors',['../group__plasma__axpy.html',1,'']]]
];
