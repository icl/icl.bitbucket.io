var searchData=
[
  ['core_20blas_20and_20auxiliary_20_28single_20core_29',['Core BLAS and Auxiliary (single core)',['../group__core__blas.html',1,'']]],
  ['cabs1_3a_20complex_201_2dnorm_20absolute_20value',['cabs1: Complex 1-norm absolute value',['../group__core__cabs1.html',1,'']]],
  ['copy_3a_20_20copy_20vector',['copy:  Copy vector',['../group__core__copy.html',1,'']]],
  ['core_5fdcabs1',['core_dcabs1',['../group__core__cabs1.html#gac2c55f034e647ade9b844cd12785b14f',1,'core_dcabs1(plasma_complex64_t alpha):&#160;core_dcabs1.c'],['../group__core__cabs1.html#gac2c55f034e647ade9b844cd12785b14f',1,'core_dcabs1(plasma_complex64_t alpha):&#160;core_dcabs1.c']]],
  ['core_5fscabs1',['core_scabs1',['../group__core__cabs1.html#ga1057209ce176ece92a56ed8330ba1484',1,'core_scabs1(plasma_complex32_t alpha):&#160;core_scabs1.c'],['../group__core__cabs1.html#ga1057209ce176ece92a56ed8330ba1484',1,'core_scabs1(plasma_complex32_t alpha):&#160;core_scabs1.c']]],
  ['ccrb2cm_3a_20converts_20tiled_20_28ccrb_29_20to_20column_2dmajor_20_28cm_29',['ccrb2cm: Converts tiled (CCRB) to column-major (CM)',['../group__plasma__ccrb2cm.html',1,'']]],
  ['cm2ccrb_3a_20converts_20column_2dmajor_20_28cm_29_20to_20tiled_20_28ccrb_29',['cm2ccrb: Converts column-major (CM) to tiled (CCRB)',['../group__plasma__cm2ccrb.html',1,'']]],
  ['copy_3a_20_20copy_20vector',['copy:  Copy vector',['../group__plasma__copy.html',1,'']]]
];
