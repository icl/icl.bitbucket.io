var searchData=
[
  ['matrix_20norms',['Matrix norms',['../group__core__norms.html',1,'']]],
  ['matrix_20layout_20conversion',['Matrix layout conversion',['../group__group__conversion.html',1,'']]],
  ['matrix_20norms',['Matrix norms',['../group__group__norms.html',1,'']]],
  ['m',['m',['../structplasma__desc__t.html#a7f9b40af027da0a8f9af989794f31a8a',1,'plasma_desc_t']]],
  ['matrix',['matrix',['../structplasma__desc__t.html#a8462ce6882591d59c14e0439a8ad60d5',1,'plasma_desc_t']]],
  ['mb',['mb',['../structplasma__desc__t.html#a8512ed221aec5b7db0e31d02acc5b5c7',1,'plasma_desc_t']]],
  ['mt',['mt',['../structplasma__desc__t.html#a35d0d7ad5f5927bb551a357a174152c2',1,'plasma_desc_t']]],
  ['map_20lapack_20_3c_3d_3e_20plasma_20constants',['Map LAPACK &lt;=&gt; PLASMA constants',['../group__plasma__const.html',1,'']]]
];
