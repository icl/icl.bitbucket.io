var searchData=
[
  ['unmlq_3a_20apply_20householder_20reflectors_20from_20lq_20to_20a_20tile',['unmlq: Apply Householder reflectors from LQ to a tile',['../group__core__unmlq.html',1,'']]],
  ['unmqr_3a_20apply_20householder_20reflectors_20from_20qr_20to_20a_20tile',['unmqr: Apply Householder reflectors from QR to a tile',['../group__core__unmqr.html',1,'']]],
  ['utilities',['Utilities',['../group__plasma__util.html',1,'']]],
  ['uplo',['uplo',['../structplasma__desc__t.html#aa5a588e5df35bdbad390497802656f9d',1,'plasma_desc_t']]]
];
