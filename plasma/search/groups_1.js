var searchData=
[
  ['asum_3a_20_20sum_20vector',['asum:  Sum vector',['../group__core__asum.html',1,'']]],
  ['axpy_3a_20_20add_20vectors',['axpy:  Add vectors',['../group__core__axpy.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__geev__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__gesv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__gesvd__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__heev__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__hesv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__posv__aux.html',1,'']]],
  ['auxiliary_20routines',['Auxiliary routines',['../group__group__qr__aux.html',1,'']]],
  ['asum_3a_20_20sum_20vector',['asum:  Sum vector',['../group__plasma__asum.html',1,'']]],
  ['axpy_3a_20_20add_20vectors',['axpy:  Add vectors',['../group__plasma__axpy.html',1,'']]]
];
