var searchData=
[
  ['core_20blas_20and_20auxiliary_20_28single_20core_29',['Core BLAS and Auxiliary (single core)',['../group__core__blas.html',1,'']]],
  ['cabs1_3a_20complex_201_2dnorm_20absolute_20value',['cabs1: Complex 1-norm absolute value',['../group__core__cabs1.html',1,'']]],
  ['copy_3a_20_20copy_20vector',['copy:  Copy vector',['../group__core__copy.html',1,'']]],
  ['ccrb2cm_3a_20converts_20tiled_20_28ccrb_29_20to_20column_2dmajor_20_28cm_29',['ccrb2cm: Converts tiled (CCRB) to column-major (CM)',['../group__plasma__ccrb2cm.html',1,'']]],
  ['cm2ccrb_3a_20converts_20column_2dmajor_20_28cm_29_20to_20tiled_20_28ccrb_29',['cm2ccrb: Converts column-major (CM) to tiled (CCRB)',['../group__plasma__cm2ccrb.html',1,'']]],
  ['copy_3a_20_20copy_20vector',['copy:  Copy vector',['../group__plasma__copy.html',1,'']]]
];
