var searchData=
[
  ['householder_20reflectors',['Householder reflectors',['../group__core__group__larf.html',1,'']]],
  ['hemm_3a_20_20hermitian_20matrix_20multiply',['hemm:  Hermitian matrix multiply',['../group__core__hemm.html',1,'']]],
  ['hemv_3a_20_20_20_20hermitian_20matrix_2dvector_20multiply',['hemv:    Hermitian matrix-vector multiply',['../group__core__hemv.html',1,'']]],
  ['her_3a_20_20_20_20_20hermitian_20rank_201_20update',['her:     Hermitian rank 1 update',['../group__core__her.html',1,'']]],
  ['her2_3a_20_20_20_20hermitian_20rank_202_20update',['her2:    Hermitian rank 2 update',['../group__core__her2.html',1,'']]],
  ['her2k_3a_20hermitian_20rank_202k_20update',['her2k: Hermitian rank 2k update',['../group__core__her2k.html',1,'']]],
  ['herk_3a_20_20hermitian_20rank_20k_20update',['herk:  Hermitian rank k update',['../group__core__herk.html',1,'']]],
  ['householder_20reflectors',['Householder reflectors',['../group__group__larf.html',1,'']]],
  ['hegst_3a_20_20divide_2dand_2dconquer_20method',['hegst:  divide-and-conquer method',['../group__plasma__hegst.html',1,'']]],
  ['hemm_3a_20_20hermitian_20matrix_20multiply',['hemm:  Hermitian matrix multiply',['../group__plasma__hemm.html',1,'']]],
  ['hemv_3a_20_20_20hermitian_20matrix_2dvector_20multiply',['hemv:   Hermitian matrix-vector multiply',['../group__plasma__hemv.html',1,'']]],
  ['her_3a_20_20_20_20hermitian_20rank_201_20update',['her:    Hermitian rank 1 update',['../group__plasma__her.html',1,'']]],
  ['her2_3a_20_20_20hermitian_20rank_202_20update',['her2:   Hermitian rank 2 update',['../group__plasma__her2.html',1,'']]],
  ['her2k_3a_20hermitian_20rank_202k_20update',['her2k: Hermitian rank 2k update',['../group__plasma__her2k.html',1,'']]],
  ['herk_3a_20_20hermitian_20rank_20k_20update',['herk:  Hermitian rank k update',['../group__plasma__herk.html',1,'']]]
];
