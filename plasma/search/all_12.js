var searchData=
[
  ['trmm_3a_20_20triangular_20matrix_20multiply',['trmm:  Triangular matrix multiply',['../group__core__trmm.html',1,'']]],
  ['trmv_3a_20_20_20_20_20_20_20triangular_20matrix_2dvector_20multiply',['trmv:       Triangular matrix-vector multiply',['../group__core__trmv.html',1,'']]],
  ['trsm_3a_20_20triangular_20solve_20matrix',['trsm:  Triangular solve matrix',['../group__core__trsm.html',1,'']]],
  ['trsv_3a_20_20_20_20_20_20_20triangular_20matrix_2dvector_20solve',['trsv:       Triangular matrix-vector solve',['../group__core__trsv.html',1,'']]],
  ['trtri_3a_20triangular_20inverse_3b_20used_20in_20getri_2c_20potri',['trtri: Triangular inverse; used in getri, potri',['../group__core__trtri.html',1,'']]],
  ['tslqt_3a_20lq_20factorization_20of_20a_20rectangular_20matrix_20of_20two_20tiles',['tslqt: LQ factorization of a rectangular matrix of two tiles',['../group__core__tslqt.html',1,'']]],
  ['tsmlq_3a_20apply_20householder_20reflectors_20from_20lq_20to_20a_20rectangular_20matrix_20of_20two_20tiles',['tsmlq: Apply Householder reflectors from LQ to a rectangular matrix of two tiles',['../group__core__tsmlq.html',1,'']]],
  ['tsmqr_3a_20apply_20householder_20reflectors_20from_20qr_20to_20a_20rectangular_20matrix_20of_20two_20tiles',['tsmqr: Apply Householder reflectors from QR to a rectangular matrix of two tiles',['../group__core__tsmqr.html',1,'']]],
  ['tsqrt_3a_20qr_20factorization_20of_20a_20rectangular_20matrix_20of_20two_20tiles',['tsqrt: QR factorization of a rectangular matrix of two tiles',['../group__core__tsqrt.html',1,'']]],
  ['trmm_3a_20_20triangular_20matrix_20multiply',['trmm:  Triangular matrix multiply',['../group__plasma__trmm.html',1,'']]],
  ['trmv_3a_20_20_20triangular_20matrix_2dvector_20multiply',['trmv:   Triangular matrix-vector multiply',['../group__plasma__trmv.html',1,'']]],
  ['trsm_3a_20_20triangular_20solve_20matrix',['trsm:  Triangular solve matrix',['../group__plasma__trsm.html',1,'']]],
  ['trsv_3a_20_20_20triangular_20matrix_2dvector_20solve',['trsv:   Triangular matrix-vector solve',['../group__plasma__trsv.html',1,'']]],
  ['trtri_3a_20triangular_20inverse_3b_20used_20in_20getri_2c_20potri',['trtri: Triangular inverse; used in getri, potri',['../group__plasma__trtri.html',1,'']]],
  ['type',['type',['../structplasma__desc__t.html#ae54acaf3d9bb42b2e1dba093ec8d66bc',1,'plasma_desc_t']]]
];
