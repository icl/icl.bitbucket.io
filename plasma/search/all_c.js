var searchData=
[
  ['nrm2_3a_20_20vector_202_20norm',['nrm2:  Vector 2 norm',['../group__core__nrm2.html',1,'']]],
  ['non_2dsymmetric_20eigenvalues',['Non-symmetric eigenvalues',['../group__group__geev.html',1,'']]],
  ['n',['n',['../structplasma__desc__t.html#a1deb8d67b1fca78449a40ba7a056cafd',1,'plasma_desc_t']]],
  ['nb',['nb',['../structplasma__desc__t.html#af3770d8c57330e1e22a149f46cddb272',1,'plasma_desc_t']]],
  ['nt',['nt',['../structplasma__desc__t.html#a7595a9db8b1b8360b5fa9726b998a117',1,'plasma_desc_t']]],
  ['nrm2_3a_20_20vector_202_20norm',['nrm2:  Vector 2 norm',['../group__plasma__nrm2.html',1,'']]]
];
