var searchData=
[
  ['orthogonal_2funitary_20factorizations',['Orthogonal/unitary factorizations',['../group__group__orthogonal.html',1,'']]],
  ['or_2fungbr_3a_20generates_20_20_20_20_20q_20or_20p_20from_20bidiagonal_20reduction',['or/ungbr: Generates     Q or P from bidiagonal reduction',['../group__plasma__ungbr.html',1,'']]],
  ['or_2funghr_3a_20generates_20_20_20_20_20q_20from_20hessenberg_20reduction',['or/unghr: Generates     Q from Hessenberg reduction',['../group__plasma__unghr.html',1,'']]],
  ['or_2funglq_3a_20generates_20_20_20_20_20q_20from_20lq_20factorization',['or/unglq: Generates     Q from LQ factorization',['../group__plasma__unglq.html',1,'']]],
  ['or_2fungql_3a_20generates_20_20_20_20_20q_20from_20ql_20factorization',['or/ungql: Generates     Q from QL factorization',['../group__plasma__ungql.html',1,'']]],
  ['or_2fungqr_3a_20generates_20_20_20_20_20q_20from_20qr_20factorization',['or/ungqr: Generates     Q from QR factorization',['../group__plasma__ungqr.html',1,'']]],
  ['or_2fungrq_3a_20generates_20_20_20_20_20q_20from_20rq_20factorization',['or/ungrq: Generates     Q from RQ factorization',['../group__plasma__ungrq.html',1,'']]],
  ['or_2fungtr_3a_20generates_20_20_20_20_20q_20from_20tridiagonal_20reduction',['or/ungtr: Generates     Q from tridiagonal reduction',['../group__plasma__ungtr.html',1,'']]],
  ['or_2funmbr_3a_20multiplies_20by_20q_20or_20p_20from_20bidiagonal_20reduction',['or/unmbr: Multiplies by Q or P from bidiagonal reduction',['../group__plasma__unmbr.html',1,'']]],
  ['or_2funmhr_3a_20multiplies_20by_20q_20from_20hessenberg_20reduction',['or/unmhr: Multiplies by Q from Hessenberg reduction',['../group__plasma__unmhr.html',1,'']]],
  ['or_2funmlq_3a_20multiplies_20by_20q_20from_20lq_20factorization',['or/unmlq: Multiplies by Q from LQ factorization',['../group__plasma__unmlq.html',1,'']]],
  ['or_2funmql_3a_20multiplies_20by_20q_20from_20ql_20factorization',['or/unmql: Multiplies by Q from QL factorization',['../group__plasma__unmql.html',1,'']]],
  ['or_2funmqr_3a_20multiplies_20by_20q_20from_20qr_20factorization',['or/unmqr: Multiplies by Q from QR factorization',['../group__plasma__unmqr.html',1,'']]],
  ['or_2funmrq_3a_20multiplies_20by_20q_20from_20rq_20factorization',['or/unmrq: Multiplies by Q from RQ factorization',['../group__plasma__unmrq.html',1,'']]],
  ['or_2funmtr_3a_20multiplies_20by_20q_20from_20tridiagonal_20reduction',['or/unmtr: Multiplies by Q from tridiagonal reduction',['../group__plasma__unmtr.html',1,'']]]
];
