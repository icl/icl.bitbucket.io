var searchData=
[
  ['precision_20conversion',['Precision conversion',['../group__core__mixed.html',1,'']]],
  ['pamm_3a_20updating_20a_20matrix_20using_20two_20tiles',['pamm: Updating a matrix using two tiles',['../group__core__pamm.html',1,'']]],
  ['parfb_3a_20apply_20householder_20reflectors_20to_20a_20rectangular_20matrix_20of_20two_20tiles',['parfb: Apply Householder reflectors to a rectangular matrix of two tiles',['../group__core__parfb.html',1,'']]],
  ['potrf_3a_20cholesky_20factorization',['potrf: Cholesky factorization',['../group__core__potrf.html',1,'']]],
  ['plasma_20blas_20and_20auxiliary_20_28parallel_29',['PLASMA BLAS and Auxiliary (parallel)',['../group__group__blas.html',1,'']]],
  ['precision_20conversion',['Precision conversion',['../group__group__mixed.html',1,'']]],
  ['plasma_20descriptor',['PLASMA descriptor',['../group__plasma__descriptor.html',1,'']]],
  ['porfs_3a_20refine_20solution',['porfs: Refine solution',['../group__plasma__porfs.html',1,'']]],
  ['posv_3a_20_20solves_20ax_20_3d_20b_20using_20cholesky_20factorization_20_28driver_29',['posv:  Solves Ax = b using Cholesky factorization (driver)',['../group__plasma__posv.html',1,'']]],
  ['potf2_3a_20cholesky_20panel_20factorization',['potf2: Cholesky panel factorization',['../group__plasma__potf2.html',1,'']]],
  ['potrf_3a_20cholesky_20factorization',['potrf: Cholesky factorization',['../group__plasma__potrf.html',1,'']]],
  ['potri_3a_20cholesky_20inverse',['potri: Cholesky inverse',['../group__plasma__potri.html',1,'']]],
  ['potrs_3a_20cholesky_20forward_20and_20back_20solves',['potrs: Cholesky forward and back solves',['../group__plasma__potrs.html',1,'']]]
];
