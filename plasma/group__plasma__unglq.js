var group__plasma__unglq =
[
    [ "plasma_cunglq", "group__plasma__unglq.html#gaa30b6f7c4230d621793cb17a1228f0a3", null ],
    [ "plasma_omp_cunglq", "group__plasma__unglq.html#ga4b955aaeffdc36974d9eb7a083f32197", null ],
    [ "plasma_dorglq", "group__plasma__unglq.html#ga907084984d3c78c346366a26cfdad83f", null ],
    [ "plasma_omp_dorglq", "group__plasma__unglq.html#gaa68a2223aac758d6b77e7174379ace43", null ],
    [ "plasma_sorglq", "group__plasma__unglq.html#gaf74bc06d94a196c5496f5b257e9f8b1d", null ],
    [ "plasma_omp_sorglq", "group__plasma__unglq.html#ga693bc0792545dd33be4b2e21742ca22c", null ],
    [ "plasma_zunglq", "group__plasma__unglq.html#gaa1d34bc998b56bf39d4399e95756237c", null ],
    [ "plasma_omp_zunglq", "group__plasma__unglq.html#ga1614d73ceb8229dbf798478948608a90", null ]
];