var group__group__ql =
[
    [ "geqlf: QL factorization", "group__plasma__geqlf.html", null ],
    [ "or/unmql: Multiplies by Q from QL factorization", "group__plasma__unmql.html", null ],
    [ "or/ungql: Generates     Q from QL factorization", "group__plasma__ungql.html", null ]
];