var group__group__solvers =
[
    [ "General matrices: LU", "group__group__gesv.html", "group__group__gesv" ],
    [ "General matrices: least squares", "group__group__gels.html", "group__group__gels" ],
    [ "Symmetric/Hermitian positive definite: Cholesky", "group__group__posv.html", "group__group__posv" ],
    [ "Symmetric/Hermitian indefinite", "group__group__hesv.html", "group__group__hesv" ]
];