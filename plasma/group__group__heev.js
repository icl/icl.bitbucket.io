var group__group__heev =
[
    [ "sy/heev:   Solves using QR iteration (driver)", "group__plasma__heev.html", null ],
    [ "sy/heevd:  Solves using divide-and-conquer (driver)", "group__plasma__heevd.html", null ],
    [ "sy/heevr:  Solves using MRRR (driver)", "group__plasma__heevr.html", null ],
    [ "sy/hetrd: Tridiagonal reduction", "group__plasma__hetrd.html", null ],
    [ "or/unmtr: Multiplies by Q from tridiagonal reduction", "group__plasma__unmtr.html", null ],
    [ "or/ungtr: Generates     Q from tridiagonal reduction", "group__plasma__ungtr.html", null ],
    [ "Auxiliary routines", "group__group__heev__aux.html", "group__group__heev__aux" ]
];