var group__plasma__hesv =
[
    [ "plasma_chesv", "group__plasma__hesv.html#gadb0febabdaa438be809366a588f09e1b", null ],
    [ "plasma_omp_chesv", "group__plasma__hesv.html#ga309729def76ed417f5b6529c004884de", null ],
    [ "plasma_dsysv", "group__plasma__hesv.html#gae9426f4b605f7c9dbbb37b6e41be4724", null ],
    [ "plasma_omp_dsysv", "group__plasma__hesv.html#ga8966bdca32f1cc75820d95b3c38f9bc5", null ],
    [ "plasma_ssysv", "group__plasma__hesv.html#ga8768ce28583adf3e8df86290c8030df8", null ],
    [ "plasma_omp_ssysv", "group__plasma__hesv.html#gaec012f04b54d5d4c5c022e394aed246e", null ],
    [ "plasma_zhesv", "group__plasma__hesv.html#ga3b32006668b1bfa762cc08812dbdae2b", null ],
    [ "plasma_omp_zhesv", "group__plasma__hesv.html#gae506efd4db016db82a4faef91a4fd4e1", null ]
];