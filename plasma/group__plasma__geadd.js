var group__plasma__geadd =
[
    [ "plasma_cgeadd", "group__plasma__geadd.html#ga081953cf098fa1a8e0158d3ab6f0ac00", null ],
    [ "plasma_omp_cgeadd", "group__plasma__geadd.html#gaf7a4125905b979b451df5dbb9c04c412", null ],
    [ "plasma_dgeadd", "group__plasma__geadd.html#ga0e634eed7213014ab76b97765f618e57", null ],
    [ "plasma_omp_dgeadd", "group__plasma__geadd.html#ga75ea83f3d65b135fabeccd7fc261eb7b", null ],
    [ "plasma_sgeadd", "group__plasma__geadd.html#gafbb5016cc81e05a0499142998a72b114", null ],
    [ "plasma_omp_sgeadd", "group__plasma__geadd.html#gaf8a2df65407ccfc9cb3d781ddd50c9a7", null ],
    [ "plasma_zgeadd", "group__plasma__geadd.html#ga100d3acf48f99ad1ea6332698d7bae79", null ],
    [ "plasma_omp_zgeadd", "group__plasma__geadd.html#ga34fb9d8dd175f1ce58ff1ec95bb24815", null ]
];