var modules =
[
    [ "Initialize/finalize", "group__plasma__init.html", "group__plasma__init" ],
    [ "PLASMA descriptor", "group__plasma__descriptor.html", "group__plasma__descriptor" ],
    [ "Utilities", "group__plasma__util.html", "group__plasma__util" ],
    [ "Matrix layout conversion", "group__group__conversion.html", "group__group__conversion" ],
    [ "Linear system solvers", "group__group__solvers.html", "group__group__solvers" ],
    [ "Orthogonal/unitary factorizations", "group__group__orthogonal.html", "group__group__orthogonal" ],
    [ "Eigenvalues", "group__group__eigenvalue.html", "group__group__eigenvalue" ],
    [ "Singular Value Decomposition (SVD)", "group__group__svd.html", "group__group__svd" ],
    [ "PLASMA BLAS and Auxiliary (parallel)", "group__group__blas.html", "group__group__blas" ],
    [ "Core BLAS and Auxiliary (single core)", "group__core__blas.html", "group__core__blas" ]
];