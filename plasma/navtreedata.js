var NAVTREE =
[
  [ "PLASMA", "index.html", [
    [ "Routines", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"group__core____dot.html",
"group__plasma__getri.html#ga32d501884d7f7207ac33abf7c21e2017",
"group__plasma__ungqr.html#ga25890ecedb7ac7fb43c0d2c1cd29ab6a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';