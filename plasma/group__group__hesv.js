var group__group__hesv =
[
    [ "sy/hesv:  Solves Ax = b using symmetric indefinite factorization (driver)", "group__plasma__hesv.html", "group__plasma__hesv" ],
    [ "sy/hetrf: Symmetric indefinite factorization", "group__plasma__hetrf.html", "group__plasma__hetrf" ],
    [ "sy/hetrs: Symmetric indefinite forward and back solves", "group__plasma__hetrs.html", "group__plasma__hetrs" ],
    [ "sy/hetri: Symmetric indefinite inverse", "group__plasma__hetri.html", null ],
    [ "sy/herfs: Refine solution", "group__plasma__herfs.html", null ],
    [ "Auxiliary routines", "group__group__hesv__aux.html", null ]
];