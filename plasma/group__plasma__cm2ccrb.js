var group__plasma__cm2ccrb =
[
    [ "plasma_omp_cge2desc", "group__plasma__cm2ccrb.html#gade3f1321ffe0524f471f3b9182fb0c3a", null ],
    [ "plasma_omp_cpb2desc", "group__plasma__cm2ccrb.html#gabd52d06d6fdb14a322d21b80951cf183", null ],
    [ "plasma_omp_ctr2desc", "group__plasma__cm2ccrb.html#gac16f5a5eda99dbd9f6051c9dcf56fe13", null ],
    [ "plasma_omp_dge2desc", "group__plasma__cm2ccrb.html#ga153a9a7ab8374c98b08e75806dc59ff3", null ],
    [ "plasma_omp_dpb2desc", "group__plasma__cm2ccrb.html#gaecbb1501485161a0ca31731367cc1ad4", null ],
    [ "plasma_omp_dtr2desc", "group__plasma__cm2ccrb.html#ga08c1901c2c2c37ad542179f2e712957e", null ],
    [ "plasma_omp_sge2desc", "group__plasma__cm2ccrb.html#ga846cad450985a2d8edff73708a6b78e2", null ],
    [ "plasma_omp_spb2desc", "group__plasma__cm2ccrb.html#ga925924052b62a8756b9af2f28c491178", null ],
    [ "plasma_omp_str2desc", "group__plasma__cm2ccrb.html#ga930d7538833b08643e835a9b041aff31", null ],
    [ "plasma_omp_zge2desc", "group__plasma__cm2ccrb.html#ga47d86c2a8f5913e3acedeb391875ea75", null ],
    [ "plasma_omp_zpb2desc", "group__plasma__cm2ccrb.html#ga4d282770995ae9e6f2f50e9af7d7bcf8", null ],
    [ "plasma_omp_ztr2desc", "group__plasma__cm2ccrb.html#gabd2254a3b33b9222b40f3bc5031758c6", null ]
];