var group__group__hegv =
[
    [ "sy/hegv:   Solves using QR iteration (driver)", "group__plasma__hegv.html", null ],
    [ "sy/hegvd:  Solves using divide-and-conquer (driver)", "group__plasma__hegvd.html", null ],
    [ "sy/hegvr:  Solves using MRRR (driver)", "group__plasma__hegvr.html", null ],
    [ "Auxiliary routines", "group__group__heev__aux.html", "group__group__heev__aux" ]
];