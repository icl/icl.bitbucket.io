var group__plasma__hetrf =
[
    [ "plasma_chetrf", "group__plasma__hetrf.html#gab65f774887aa324af0cec731ef46a0bb", null ],
    [ "plasma_omp_chetrf", "group__plasma__hetrf.html#gae262d400801bdee6c59747486f79435c", null ],
    [ "plasma_dsytrf", "group__plasma__hetrf.html#gaec329cb1a0078b692d03aab2f90910bc", null ],
    [ "plasma_omp_dsytrf", "group__plasma__hetrf.html#gad3280e0b6b5f237667be86344959c367", null ],
    [ "plasma_ssytrf", "group__plasma__hetrf.html#gac2bdf537a41260c99cddfda73779cc39", null ],
    [ "plasma_omp_ssytrf", "group__plasma__hetrf.html#ga61b801be6f68473d9bc204183b661d9f", null ],
    [ "plasma_zhetrf", "group__plasma__hetrf.html#ga628e17a35ae3f1bf57520613ff3c5e23", null ],
    [ "plasma_omp_zhetrf", "group__plasma__hetrf.html#gac86a530fa48e36163dc44407a26441cd", null ]
];