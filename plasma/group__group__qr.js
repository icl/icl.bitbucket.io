var group__group__qr =
[
    [ "geqrf: QR factorization", "group__plasma__geqrf.html", "group__plasma__geqrf" ],
    [ "or/unmqr: Multiplies by Q from QR factorization", "group__plasma__unmqr.html", "group__plasma__unmqr" ],
    [ "or/ungqr: Generates     Q from QR factorization", "group__plasma__ungqr.html", "group__plasma__ungqr" ],
    [ "Auxiliary routines", "group__group__qr__aux.html", "group__group__qr__aux" ]
];