var group__group__geev =
[
    [ "geev:  Non-symmetric eigenvalues (driver)", "group__plasma__geev.html", null ],
    [ "gehrd: Hessenberg reduction", "group__plasma__gehrd.html", null ],
    [ "or/unmhr: Multiplies by Q from Hessenberg reduction", "group__plasma__unmhr.html", null ],
    [ "or/unghr: Generates     Q from Hessenberg reduction", "group__plasma__unghr.html", null ],
    [ "Auxiliary routines", "group__group__geev__aux.html", null ]
];