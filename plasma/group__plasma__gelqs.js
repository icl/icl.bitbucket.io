var group__plasma__gelqs =
[
    [ "plasma_cgelqs", "group__plasma__gelqs.html#ga131c0387955a69418f15f0b14e246f4b", null ],
    [ "plasma_omp_cgelqs", "group__plasma__gelqs.html#gade008bafea294bec3bc41b42a1606ac8", null ],
    [ "plasma_dgelqs", "group__plasma__gelqs.html#ga563f9f2c611bef095c1a5ebc70a460bb", null ],
    [ "plasma_omp_dgelqs", "group__plasma__gelqs.html#ga9700c9ac95f980650e377d7766ee27a1", null ],
    [ "plasma_sgelqs", "group__plasma__gelqs.html#ga6a6c388c8c71f6c17fabf8ba48af8a6a", null ],
    [ "plasma_omp_sgelqs", "group__plasma__gelqs.html#ga8e960e6f4f7f058ce80633dd39f34a12", null ],
    [ "plasma_zgelqs", "group__plasma__gelqs.html#gae68845bd4c1f28e756a643a319d56b05", null ],
    [ "plasma_omp_zgelqs", "group__plasma__gelqs.html#gae96887d8e9188b910b4bc814760a8c45", null ]
];