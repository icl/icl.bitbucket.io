var group__plasma__gemm =
[
    [ "plasma_cgemm", "group__plasma__gemm.html#gac8cd244912a29e65692c54567774bc5e", null ],
    [ "plasma_omp_cgemm", "group__plasma__gemm.html#ga25d89a647c5464e4391a4ed498904035", null ],
    [ "plasma_dgemm", "group__plasma__gemm.html#ga3de7542eaf89ca1a58f3f9a40e1ac9a7", null ],
    [ "plasma_omp_dgemm", "group__plasma__gemm.html#gaf7390c6718a8e4f37b63d017d032bb7b", null ],
    [ "plasma_sgemm", "group__plasma__gemm.html#ga4dac59ccafa563c5396aef572d3f6119", null ],
    [ "plasma_omp_sgemm", "group__plasma__gemm.html#ga9f8a1cbaf04c869496876e53e67f022e", null ],
    [ "plasma_zgemm", "group__plasma__gemm.html#gaa147332e4c4b43f1b0520521fcc7daab", null ],
    [ "plasma_omp_zgemm", "group__plasma__gemm.html#gaeb8eff9934ac5c25280094237f21d8e6", null ]
];