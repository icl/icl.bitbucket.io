var group__plasma__potrf =
[
    [ "plasma_cpotrf", "group__plasma__potrf.html#ga7dbebe3c56cdf7ea8c93b8f6848eff53", null ],
    [ "plasma_omp_cpotrf", "group__plasma__potrf.html#ga2ec63f88f862d8863959a6e5ff273e91", null ],
    [ "plasma_dpotrf", "group__plasma__potrf.html#gad2d1008ced4c84e270e0164c7db3a3ca", null ],
    [ "plasma_omp_dpotrf", "group__plasma__potrf.html#ga4eed62a5f014b30895f49d701cecf559", null ],
    [ "plasma_spotrf", "group__plasma__potrf.html#ga6ea2b49cef338e6099711da3943ef143", null ],
    [ "plasma_omp_spotrf", "group__plasma__potrf.html#gafa3e35d83dc4bde45127526205317894", null ],
    [ "plasma_zpotrf", "group__plasma__potrf.html#ga8388e7db1701fd5a2032c11dfc4b3e5f", null ],
    [ "plasma_omp_zpotrf", "group__plasma__potrf.html#ga2e1393e2520e827bb49e55558037cff0", null ]
];