var group__plasma__geqrs =
[
    [ "plasma_cgeqrs", "group__plasma__geqrs.html#gaadd43a3ba50213d7b390fb0addb9f719", null ],
    [ "plasma_omp_cgeqrs", "group__plasma__geqrs.html#gaf5e79c41cc40badefa93a93eee013e80", null ],
    [ "plasma_dgeqrs", "group__plasma__geqrs.html#ga9aebdb65f06a09f05a7149df580c4f0c", null ],
    [ "plasma_omp_dgeqrs", "group__plasma__geqrs.html#ga9c41e693aa3a6781102d73ec0dcd086e", null ],
    [ "plasma_sgeqrs", "group__plasma__geqrs.html#ga4e36379788e3e9781eebae7ae8137ca6", null ],
    [ "plasma_omp_sgeqrs", "group__plasma__geqrs.html#gac9e2fa8896d44bd16791a0dff39f1e2b", null ],
    [ "plasma_zgeqrs", "group__plasma__geqrs.html#gaa4b7ade24a67aa375f4ed4ff7bf855b3", null ],
    [ "plasma_omp_zgeqrs", "group__plasma__geqrs.html#ga7670bdc55636fcd7dc7c9cc7dc6b5591", null ]
];