var group__core__solvers =
[
    [ "potrf: Cholesky factorization", "group__core__potrf.html", "group__core__potrf" ],
    [ "geqrt: QR factorization of a tile", "group__core__geqrt.html", "group__core__geqrt" ],
    [ "tsqrt: QR factorization of a rectangular matrix of two tiles", "group__core__tsqrt.html", "group__core__tsqrt" ],
    [ "unmqr: Apply Householder reflectors from QR to a tile", "group__core__unmqr.html", "group__core__unmqr" ],
    [ "tsmqr: Apply Householder reflectors from QR to a rectangular matrix of two tiles", "group__core__tsmqr.html", "group__core__tsmqr" ],
    [ "gelqt: LQ factorization of a tile", "group__core__gelqt.html", "group__core__gelqt" ],
    [ "tslqt: LQ factorization of a rectangular matrix of two tiles", "group__core__tslqt.html", "group__core__tslqt" ],
    [ "unmlq: Apply Householder reflectors from LQ to a tile", "group__core__unmlq.html", "group__core__unmlq" ],
    [ "tsmlq: Apply Householder reflectors from LQ to a rectangular matrix of two tiles", "group__core__tsmlq.html", "group__core__tsmlq" ],
    [ "pamm: Updating a matrix using two tiles", "group__core__pamm.html", "group__core__pamm" ],
    [ "parfb: Apply Householder reflectors to a rectangular matrix of two tiles", "group__core__parfb.html", "group__core__parfb" ]
];