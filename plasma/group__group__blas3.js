var group__group__blas3 =
[
    [ "gemm:  General matrix multiply: C = AB + C", "group__plasma__gemm.html", "group__plasma__gemm" ],
    [ "hemm:  Hermitian matrix multiply", "group__plasma__hemm.html", "group__plasma__hemm" ],
    [ "herk:  Hermitian rank k update", "group__plasma__herk.html", "group__plasma__herk" ],
    [ "her2k: Hermitian rank 2k update", "group__plasma__her2k.html", "group__plasma__her2k" ],
    [ "symm:  Symmetric matrix multiply", "group__plasma__symm.html", "group__plasma__symm" ],
    [ "syrk:  Symmetric rank k update", "group__plasma__syrk.html", "group__plasma__syrk" ],
    [ "syr2k: Symmetric rank 2k update", "group__plasma__syr2k.html", "group__plasma__syr2k" ],
    [ "trmm:  Triangular matrix multiply", "group__plasma__trmm.html", "group__plasma__trmm" ],
    [ "trsm:  Triangular solve matrix", "group__plasma__trsm.html", "group__plasma__trsm" ],
    [ "trtri: Triangular inverse; used in getri, potri", "group__plasma__trtri.html", "group__plasma__trtri" ]
];