var group__plasma__syrk =
[
    [ "plasma_csyrk", "group__plasma__syrk.html#ga4679963662b6b0d2bbabfcbc3f023fbf", null ],
    [ "plasma_omp_csyrk", "group__plasma__syrk.html#gada746e2438193ec8e16f1367adc8aaac", null ],
    [ "plasma_dsyrk", "group__plasma__syrk.html#ga323bb413ec7ff5f5bbe694617a40008a", null ],
    [ "plasma_omp_dsyrk", "group__plasma__syrk.html#ga4879bf46f777b18e123c2e766615a91e", null ],
    [ "plasma_ssyrk", "group__plasma__syrk.html#ga17da4b851d2207444bf60ac32db0117f", null ],
    [ "plasma_omp_ssyrk", "group__plasma__syrk.html#gaeef1ebcf32ca518d263882decacb00ea", null ],
    [ "plasma_zsyrk", "group__plasma__syrk.html#ga3af846892e872aa6b17628f30806dacc", null ],
    [ "plasma_omp_zsyrk", "group__plasma__syrk.html#ga3475193789c4ee2158aebf4e66d4d02f", null ]
];