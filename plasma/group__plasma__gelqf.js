var group__plasma__gelqf =
[
    [ "plasma_cgelqf", "group__plasma__gelqf.html#ga0f67ee724513ceae01ded32f82145f4c", null ],
    [ "plasma_omp_cgelqf", "group__plasma__gelqf.html#ga68fb3cbc080351c7f1283111560b04b4", null ],
    [ "plasma_dgelqf", "group__plasma__gelqf.html#ga9cca58ee7248328abd6f2ba4535939a6", null ],
    [ "plasma_omp_dgelqf", "group__plasma__gelqf.html#ga61a0c8489276c8ae251021ddc0ec0cda", null ],
    [ "plasma_sgelqf", "group__plasma__gelqf.html#gaf3f48844e3d45823746151f3899ad1a1", null ],
    [ "plasma_omp_sgelqf", "group__plasma__gelqf.html#ga3c755af4dcf5f7eeb3597910c41d0ac4", null ],
    [ "plasma_zgelqf", "group__plasma__gelqf.html#gaa4ef5b579e6699eb1e92f7094fe43f1e", null ],
    [ "plasma_omp_zgelqf", "group__plasma__gelqf.html#ga6768d513b9e5422fd76a729a11e84b0c", null ]
];