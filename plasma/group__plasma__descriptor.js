var group__plasma__descriptor =
[
    [ "plasma_desc_t", "structplasma__desc__t.html", [
      [ "type", "structplasma__desc__t.html#ae54acaf3d9bb42b2e1dba093ec8d66bc", null ],
      [ "uplo", "structplasma__desc__t.html#aa5a588e5df35bdbad390497802656f9d", null ],
      [ "precision", "structplasma__desc__t.html#aada7517ac988696d7c32f968c7f3721e", null ],
      [ "matrix", "structplasma__desc__t.html#a8462ce6882591d59c14e0439a8ad60d5", null ],
      [ "A21", "structplasma__desc__t.html#a9749348dffa9c3584312c581f4c2bc20", null ],
      [ "A12", "structplasma__desc__t.html#a5081e56a01179e8b9305912676909a13", null ],
      [ "A22", "structplasma__desc__t.html#ad8485dcb9895aee64a447c70642cb554", null ],
      [ "mb", "structplasma__desc__t.html#a8512ed221aec5b7db0e31d02acc5b5c7", null ],
      [ "nb", "structplasma__desc__t.html#af3770d8c57330e1e22a149f46cddb272", null ],
      [ "gm", "structplasma__desc__t.html#af30edbca828d7aa1ba97836774f6cdf4", null ],
      [ "gn", "structplasma__desc__t.html#afc7fdb4829c75a913e9e8c4d91c3f72b", null ],
      [ "gmt", "structplasma__desc__t.html#aa0ffc4c98d7a9fe7fd8de85995c262d5", null ],
      [ "gnt", "structplasma__desc__t.html#a8bbbc1c4590ed611e0f8de0abba5c992", null ],
      [ "i", "structplasma__desc__t.html#a7508194cedb3083d9152c4e2f957dc88", null ],
      [ "j", "structplasma__desc__t.html#a1d5e8aa30ce545ac2d9c2e75e201e976", null ],
      [ "m", "structplasma__desc__t.html#a7f9b40af027da0a8f9af989794f31a8a", null ],
      [ "n", "structplasma__desc__t.html#a1deb8d67b1fca78449a40ba7a056cafd", null ],
      [ "mt", "structplasma__desc__t.html#a35d0d7ad5f5927bb551a357a174152c2", null ],
      [ "nt", "structplasma__desc__t.html#a7595a9db8b1b8360b5fa9726b998a117", null ],
      [ "kl", "structplasma__desc__t.html#aed395e85805db91b480fe052ccb103a2", null ],
      [ "ku", "structplasma__desc__t.html#aea044dd5c95b243435085a87cc0f8b01", null ],
      [ "klt", "structplasma__desc__t.html#a421b54d36d7c0dbc53565142dab5f605", null ],
      [ "kut", "structplasma__desc__t.html#af974690ceb88d10c1887f05ea83f8d73", null ]
    ] ]
];