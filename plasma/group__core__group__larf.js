var group__core__group__larf =
[
    [ "larf:  Apply Householder reflector to general matrix", "group__core__larf.html", null ],
    [ "larfy: Apply Householder reflector to symmetric/Hermitian matrix", "group__core__larfy.html", null ],
    [ "larfg: Generate Householder reflector", "group__core__larfg.html", null ],
    [ "larfb: Apply block of Householder reflectors (Level 3)", "group__core__larfb.html", null ]
];