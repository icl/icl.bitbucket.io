var group__group__norms =
[
    [ "lange:    General matrix norm", "group__plasma__lange.html", "group__plasma__lange" ],
    [ "lansy/he: Symmetric/Hermitian matrix norm", "group__plasma__lanhe.html", "group__plasma__lanhe" ],
    [ "lantr:    Triangular matrix norm", "group__plasma__lantr.html", "group__plasma__lantr" ]
];