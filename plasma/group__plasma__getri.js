var group__plasma__getri =
[
    [ "plasma_cgetri", "group__plasma__getri.html#ga306536364fcd0d610d8e5c648e2b0b74", null ],
    [ "plasma_cgetri_aux", "group__plasma__getri.html#gab6b22fae7b9870fc70ec3c2600dda58e", null ],
    [ "plasma_omp_cgetri_aux", "group__plasma__getri.html#gac17296b617b5d4879b5059f2acca1786", null ],
    [ "plasma_dgetri", "group__plasma__getri.html#gaaa7ab8cadd70ee63b2526516e99da748", null ],
    [ "plasma_dgetri_aux", "group__plasma__getri.html#ga1ebefdcdd548aaf3701bf6b4d187fa54", null ],
    [ "plasma_omp_dgetri_aux", "group__plasma__getri.html#gae1aea8653f8e38832ddeb23a574f5cf7", null ],
    [ "plasma_sgetri", "group__plasma__getri.html#ga46ac58218515ffe2bd91a85a773c9394", null ],
    [ "plasma_sgetri_aux", "group__plasma__getri.html#ga93e2611b1e461222df37f05087f01937", null ],
    [ "plasma_omp_sgetri_aux", "group__plasma__getri.html#ga194671abf8678cdc4c7ad4cb470a2f0f", null ],
    [ "plasma_zgetri", "group__plasma__getri.html#ga9298df5716115cf79a818020d35693b9", null ],
    [ "plasma_zgetri_aux", "group__plasma__getri.html#ga32d501884d7f7207ac33abf7c21e2017", null ],
    [ "plasma_omp_zgetri_aux", "group__plasma__getri.html#gaa5e2f28cdbfe142d2f29f736f14abf93", null ]
];