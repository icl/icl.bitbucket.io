var group__group__conversion =
[
    [ "cm2ccrb: Converts column-major (CM) to tiled (CCRB)", "group__plasma__cm2ccrb.html", "group__plasma__cm2ccrb" ],
    [ "ccrb2cm: Converts tiled (CCRB) to column-major (CM)", "group__plasma__ccrb2cm.html", "group__plasma__ccrb2cm" ]
];