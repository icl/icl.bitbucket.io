var group__plasma__hetrs =
[
    [ "plasma_chetrs", "group__plasma__hetrs.html#gad5e17ffc35ea4b49e05a447d0dd67cdf", null ],
    [ "plasma_omp_chetrs", "group__plasma__hetrs.html#ga2983a70dcdf5d81308023b19502dd02b", null ],
    [ "plasma_dsytrs", "group__plasma__hetrs.html#gab563256959e0c5eb2871d91b8fd13574", null ],
    [ "plasma_omp_dsytrs", "group__plasma__hetrs.html#ga4c71bb2c6fc6a0804fa58c065a476694", null ],
    [ "plasma_ssytrs", "group__plasma__hetrs.html#gaa20fe4ec4e5740bb52fd001b4c2e0971", null ],
    [ "plasma_omp_ssytrs", "group__plasma__hetrs.html#ga4559f038186dcfd00211d83904a13a0f", null ],
    [ "plasma_zhetrs", "group__plasma__hetrs.html#gaef0a4fad45d28d8676734aacd388c889", null ],
    [ "plasma_omp_zhetrs", "group__plasma__hetrs.html#gac39c231ef825412c8d5df996f6ac49ab", null ]
];