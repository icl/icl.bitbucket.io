var group__plasma__ungqr =
[
    [ "plasma_cungqr", "group__plasma__ungqr.html#gab21973db047afbef76633025e7089e83", null ],
    [ "plasma_omp_cungqr", "group__plasma__ungqr.html#ga303d757008b1012869679e5a7223e4b6", null ],
    [ "plasma_dorgqr", "group__plasma__ungqr.html#ga2a762df30d95e3ea150a3755e70eaf80", null ],
    [ "plasma_omp_dorgqr", "group__plasma__ungqr.html#gadae1a8387eb730659657b24b7b62cedd", null ],
    [ "plasma_sorgqr", "group__plasma__ungqr.html#ga25890ecedb7ac7fb43c0d2c1cd29ab6a", null ],
    [ "plasma_omp_sorgqr", "group__plasma__ungqr.html#gafcf67408189a68f4eeb8d1f5bd8ba595", null ],
    [ "plasma_zungqr", "group__plasma__ungqr.html#ga1cf8b8ad18741cda608d6d6935c350b2", null ],
    [ "plasma_omp_zungqr", "group__plasma__ungqr.html#ga339e68ae794c77eaf0d1d3d16c66de68", null ]
];