var group__plasma__symm =
[
    [ "plasma_csymm", "group__plasma__symm.html#ga469dc1d0bfff61654d32a2878a71e585", null ],
    [ "plasma_omp_csymm", "group__plasma__symm.html#ga08e3c8b4a779733ecbdba5459d767256", null ],
    [ "plasma_dsymm", "group__plasma__symm.html#gaa0569fa550021f7997fa67ad16830024", null ],
    [ "plasma_omp_dsymm", "group__plasma__symm.html#ga0d9cb32683b9029124a844dddab32033", null ],
    [ "plasma_ssymm", "group__plasma__symm.html#gad171b1a443e8342e0b4720d997416233", null ],
    [ "plasma_omp_ssymm", "group__plasma__symm.html#gaa6bf0d0601cf29bc9b68edb88cfa774b", null ],
    [ "plasma_zsymm", "group__plasma__symm.html#ga031f0bbcf7e509b1639b69082f8f1af3", null ],
    [ "plasma_omp_zsymm", "group__plasma__symm.html#ga84796d5bd57273034893035a36b0d2ab", null ]
];