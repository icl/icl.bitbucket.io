var group__plasma__potrs =
[
    [ "plasma_cpotrs", "group__plasma__potrs.html#ga67103e8755950d9d247c6a91ce09832d", null ],
    [ "plasma_omp_cpotrs", "group__plasma__potrs.html#ga9b78c36776ce850f41326433d0c07300", null ],
    [ "plasma_dpotrs", "group__plasma__potrs.html#ga518557d95b94ea58125132b43d2e926c", null ],
    [ "plasma_omp_dpotrs", "group__plasma__potrs.html#ga91b43499d2eebcfe98330de00599a28c", null ],
    [ "plasma_spotrs", "group__plasma__potrs.html#ga8d876a28a78366dbd7ababc46d5c7110", null ],
    [ "plasma_omp_spotrs", "group__plasma__potrs.html#gad0081012190cabfb9834e5577f32c9c5", null ],
    [ "plasma_zpotrs", "group__plasma__potrs.html#ga8a11c4b283720612403674e6d8795e45", null ],
    [ "plasma_omp_zpotrs", "group__plasma__potrs.html#ga12c762d1d6e41fe533603c075710afca", null ]
];