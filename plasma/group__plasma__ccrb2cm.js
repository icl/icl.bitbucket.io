var group__plasma__ccrb2cm =
[
    [ "plasma_omp_cdesc2ge", "group__plasma__ccrb2cm.html#ga12962899b11e31ebc8c24e00f80f604f", null ],
    [ "plasma_omp_cdesc2pb", "group__plasma__ccrb2cm.html#gadeb5fd877bab7285d1571daceffcc03a", null ],
    [ "plasma_omp_cdesc2tr", "group__plasma__ccrb2cm.html#ga94d1da8935f0d5c62dcd78b20c662dc0", null ],
    [ "plasma_omp_ddesc2ge", "group__plasma__ccrb2cm.html#gaf852e337f4c7f474205d186dff186c0e", null ],
    [ "plasma_omp_ddesc2pb", "group__plasma__ccrb2cm.html#gaa35ddd582c86e9fa71eb212db9c3537f", null ],
    [ "plasma_omp_ddesc2tr", "group__plasma__ccrb2cm.html#gaacf269e4afef9cc1fe1889fe5da1162a", null ],
    [ "plasma_omp_sdesc2ge", "group__plasma__ccrb2cm.html#ga09d7e8cd29fc03faa2ab34cd50459507", null ],
    [ "plasma_omp_sdesc2pb", "group__plasma__ccrb2cm.html#ga1f0654302786de497a6427e2b8f52af3", null ],
    [ "plasma_omp_sdesc2tr", "group__plasma__ccrb2cm.html#ga83e906f85a80c0cffaec6788bf478307", null ],
    [ "plasma_omp_zdesc2ge", "group__plasma__ccrb2cm.html#gacc15bf5d6ad3173038074195d7478720", null ],
    [ "plasma_omp_zdesc2pb", "group__plasma__ccrb2cm.html#gad7358a51625b2b3a1883ddae2dd42e39", null ],
    [ "plasma_omp_zdesc2tr", "group__plasma__ccrb2cm.html#ga572712d8e7c781a602e4b2df359ff104", null ]
];