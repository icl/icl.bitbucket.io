var group__group__lq =
[
    [ "gelqf: LQ factorization", "group__plasma__gelqf.html", "group__plasma__gelqf" ],
    [ "or/unmlq: Multiplies by Q from LQ factorization", "group__plasma__unmlq.html", "group__plasma__unmlq" ],
    [ "or/unglq: Generates     Q from LQ factorization", "group__plasma__unglq.html", "group__plasma__unglq" ]
];