var group__group__blas1 =
[
    [ "asum:  Sum vector", "group__plasma__asum.html", null ],
    [ "axpy:  Add vectors", "group__plasma__axpy.html", null ],
    [ "copy:  Copy vector", "group__plasma__copy.html", null ],
    [ "dot:   Dot (inner) product", "group__plasma____dot.html", null ],
    [ "iamax: Find max element", "group__plasma__iamax.html", null ],
    [ "iamin: Find min element", "group__plasma__iamin.html", null ],
    [ "nrm2:  Vector 2 norm", "group__plasma__nrm2.html", null ],
    [ "rot:   Apply Given's rotation", "group__plasma__rot.html", null ],
    [ "rotg:  Generate Given's rotation", "group__plasma__rotg.html", null ],
    [ "scal:  Scale vector", "group__plasma__scal.html", null ],
    [ "swap:  Swap vectors", "group__plasma__swap.html", null ]
];