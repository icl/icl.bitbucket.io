var group__group__posv =
[
    [ "posv:  Solves Ax = b using Cholesky factorization (driver)", "group__plasma__posv.html", "group__plasma__posv" ],
    [ "potrf: Cholesky factorization", "group__plasma__potrf.html", "group__plasma__potrf" ],
    [ "potrs: Cholesky forward and back solves", "group__plasma__potrs.html", "group__plasma__potrs" ],
    [ "potri: Cholesky inverse", "group__plasma__potri.html", "group__plasma__potri" ],
    [ "porfs: Refine solution", "group__plasma__porfs.html", null ],
    [ "Auxiliary routines", "group__group__posv__aux.html", "group__group__posv__aux" ]
];