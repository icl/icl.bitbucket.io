var group__core__blas =
[
    [ "Level 0: element operations, O(1) work", "group__core__blas0.html", "group__core__blas0" ],
    [ "Level 1: vectors operations, O(n) work", "group__core__blas1.html", "group__core__blas1" ],
    [ "Level 2: matrix-vector operations, O(n^2) work", "group__core__blas2.html", "group__core__blas2" ],
    [ "Level 3: matrix-matrix operations, O(n^3) work", "group__core__blas3.html", "group__core__blas3" ],
    [ "Householder reflectors", "group__core__group__larf.html", "group__core__group__larf" ],
    [ "Precision conversion", "group__core__mixed.html", "group__core__mixed" ],
    [ "Matrix norms", "group__core__norms.html", "group__core__norms" ],
    [ "Linear system solvers", "group__core__solvers.html", "group__core__solvers" ]
];