var group__plasma__gels =
[
    [ "plasma_cgels", "group__plasma__gels.html#ga559d35780411ee2041bc662c4917d939", null ],
    [ "plasma_omp_cgels", "group__plasma__gels.html#ga4c1ee2123b1634fbb2f6a55737dd3c02", null ],
    [ "plasma_dgels", "group__plasma__gels.html#ga3c04ae5952faece2635eb867195c07e8", null ],
    [ "plasma_omp_dgels", "group__plasma__gels.html#ga7f6da1f7c8edfd9d45566979966f1a6b", null ],
    [ "plasma_sgels", "group__plasma__gels.html#ga4dc075ccfe101c8ba73e25f11fed4816", null ],
    [ "plasma_omp_sgels", "group__plasma__gels.html#ga7b0b2818386944d207d256a2d4a9bbe0", null ],
    [ "plasma_zgels", "group__plasma__gels.html#gafa44cbfc5762c6d0b8979f5dc7b5b133", null ],
    [ "plasma_omp_zgels", "group__plasma__gels.html#gae6a2cd9ab3a256ea20dc224c4c91dddc", null ]
];