var group__core__blas1 =
[
    [ "asum:  Sum vector", "group__core__asum.html", null ],
    [ "axpy:  Add vectors", "group__core__axpy.html", null ],
    [ "copy:  Copy vector", "group__core__copy.html", null ],
    [ "dot:   Dot (inner) product", "group__core____dot.html", null ],
    [ "iamax: Find max element", "group__core__iamax.html", null ],
    [ "iamin: Find min element", "group__core__iamin.html", null ],
    [ "nrm2:  Vector 2 norm", "group__core__nrm2.html", null ],
    [ "rot:   Apply Given's rotation", "group__core__rot.html", null ],
    [ "rotg:  Generate Given's rotation", "group__core__rotg.html", null ],
    [ "scal:  Scale vector", "group__core__scal.html", null ],
    [ "swap:  Swap vectors", "group__core__swap.html", null ]
];