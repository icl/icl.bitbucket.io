var group__core__blas3 =
[
    [ "gemm:  General matrix multiply: C = AB + C", "group__core__gemm.html", "group__core__gemm" ],
    [ "hemm:  Hermitian matrix multiply", "group__core__hemm.html", "group__core__hemm" ],
    [ "herk:  Hermitian rank k update", "group__core__herk.html", "group__core__herk" ],
    [ "her2k: Hermitian rank 2k update", "group__core__her2k.html", "group__core__her2k" ],
    [ "symm:  Symmetric matrix multiply", "group__core__symm.html", "group__core__symm" ],
    [ "syrk:  Symmetric rank k update", "group__core__syrk.html", "group__core__syrk" ],
    [ "syr2k: Symmetric rank 2k update", "group__core__syr2k.html", "group__core__syr2k" ],
    [ "trmm:  Triangular matrix multiply", "group__core__trmm.html", "group__core__trmm" ],
    [ "trsm:  Triangular solve matrix", "group__core__trsm.html", "group__core__trsm" ],
    [ "trtri: Triangular inverse; used in getri, potri", "group__core__trtri.html", "group__core__trtri" ]
];