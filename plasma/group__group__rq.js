var group__group__rq =
[
    [ "gerqf: RQ factorization", "group__plasma__gerqf.html", null ],
    [ "or/unmrq: Multiplies by Q from RQ factorization", "group__plasma__unmrq.html", null ],
    [ "or/ungrq: Generates     Q from RQ factorization", "group__plasma__ungrq.html", null ]
];