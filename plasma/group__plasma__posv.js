var group__plasma__posv =
[
    [ "plasma_cposv", "group__plasma__posv.html#ga9c942f7ab599725233f57220b179ac82", null ],
    [ "plasma_omp_cposv", "group__plasma__posv.html#gac54949a7c9c071d732e3a3cb51e33800", null ],
    [ "plasma_dposv", "group__plasma__posv.html#ga5fba6921263c4849be86f53d89361d02", null ],
    [ "plasma_omp_dposv", "group__plasma__posv.html#ga75251062b3c51e7eec1b786d8149ba24", null ],
    [ "plasma_dsposv", "group__plasma__posv.html#ga24ea32cefe47b187b9f6bdbcd517fd7a", null ],
    [ "plasma_omp_dsposv", "group__plasma__posv.html#gabc2e8bb16ebe300f281cc0299df8c49b", null ],
    [ "plasma_sposv", "group__plasma__posv.html#gac072e337e1953b2dc6dfd39161c864c6", null ],
    [ "plasma_omp_sposv", "group__plasma__posv.html#ga934241fca6eff524ae1e45fc3ff04968", null ],
    [ "plasma_zcposv", "group__plasma__posv.html#gac28c12f5a1cfe7b4b8b1a8d97039bdae", null ],
    [ "plasma_omp_zcposv", "group__plasma__posv.html#gae7299d355de8251db8ca8862f3015002", null ],
    [ "plasma_zposv", "group__plasma__posv.html#ga770a1737f2e294ad0a1fec5b362e4ca2", null ],
    [ "plasma_omp_zposv", "group__plasma__posv.html#ga0fe04f7cfd333612eef8f69034dd702b", null ]
];