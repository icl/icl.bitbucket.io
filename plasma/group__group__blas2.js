var group__group__blas2 =
[
    [ "geadd: Add matrices", "group__plasma__geadd.html", "group__plasma__geadd" ],
    [ "gemv:   General matrix-vector multiply", "group__plasma__gemv.html", null ],
    [ "ger:    General matrix rank 1 update", "group__plasma__ger.html", null ],
    [ "hemv:   Hermitian matrix-vector multiply", "group__plasma__hemv.html", null ],
    [ "her:    Hermitian rank 1 update", "group__plasma__her.html", null ],
    [ "her2:   Hermitian rank 2 update", "group__plasma__her2.html", null ],
    [ "symv:   Symmetric matrix-vector multiply", "group__plasma__symv.html", null ],
    [ "syr:    Symmetric rank 1 update", "group__plasma__syr.html", null ],
    [ "syr2:   Symmetric rank 2 update", "group__plasma__syr2.html", null ],
    [ "trmv:   Triangular matrix-vector multiply", "group__plasma__trmv.html", null ],
    [ "trsv:   Triangular matrix-vector solve", "group__plasma__trsv.html", null ],
    [ "lacpy:  Copy matrix", "group__plasma__lacpy.html", "group__plasma__lacpy" ],
    [ "lascl:  Scale matrix by scalar", "group__plasma__lascl.html", null ],
    [ "lascl2: Scale matrix by diagonal", "group__plasma__lascl2.html", null ],
    [ "laset:  Set matrix to constants", "group__plasma__laset.html", null ]
];