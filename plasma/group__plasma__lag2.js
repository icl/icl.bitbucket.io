var group__plasma__lag2 =
[
    [ "plasma_clag2z", "group__plasma__lag2.html#ga8cc57f9b4b431affb5d2d432068db98d", null ],
    [ "plasma_omp_clag2z", "group__plasma__lag2.html#gaf36af0c7dab7b437e60b091dffd80458", null ],
    [ "plasma_dlag2s", "group__plasma__lag2.html#gaca02144a226ec0e285ce9745cf8f4e14", null ],
    [ "plasma_omp_dlag2s", "group__plasma__lag2.html#gabf6a9b9492b23564baff3e71f032d921", null ],
    [ "plasma_slag2d", "group__plasma__lag2.html#ga28b19c5803042e35f102059518b59b7d", null ],
    [ "plasma_omp_slag2d", "group__plasma__lag2.html#ga8b8d35f9a0acf65ecc2e59a6bca61a95", null ],
    [ "plasma_zlag2c", "group__plasma__lag2.html#gaff3b1f2227dc61549b2d932748869d86", null ],
    [ "plasma_omp_zlag2c", "group__plasma__lag2.html#gaf32aed5f2b19137cd73931e49018aff4", null ]
];