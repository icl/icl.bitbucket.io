var group__group__symmetric__eigen =
[
    [ "Standard, AV = V Lambda", "group__heev.html", "group__heev" ],
    [ "Standard, AV = V Lambda: packed", "group__hpev.html", null ],
    [ "Standard, AV = V Lambda: banded", "group__hbev.html", null ],
    [ "Standard, AV = V Lambda: tridiagonal", "group__htev.html", null ],
    [ "Generalized, AV = BV Lambda, etc.", "group__hygv.html", null ],
    [ "Generalized, AV = BV Lambda, etc.: packed", "group__hpgv.html", null ],
    [ "Generalized, AV = BV Lambda, etc.: banded", "group__hbgv.html", null ],
    [ "Computational routines", "group__heev__computational.html", "group__heev__computational" ]
];