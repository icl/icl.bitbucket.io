var group__norm =
[
    [ "lapack::langb", "group__norm.html#ga5365fb3bd54a10b0a456368978ae8177", null ],
    [ "lapack::lange", "group__norm.html#ga4112b8d321600e9b1b433f1d3f44c996", null ],
    [ "lapack::langt", "group__norm.html#ga0bc9ec390a380ca2a5f8112ebe37dc49", null ],
    [ "lapack::lanhb", "group__norm.html#ga7b5d693bf78f8e27defb7258b6942ef5", null ],
    [ "lapack::lanhe", "group__norm.html#gaa8869455e2d3e7c481743e61ec567c07", null ],
    [ "lapack::lanhp", "group__norm.html#gaa9622d220a8dbc4bf88dbcfa177c0bdf", null ],
    [ "lapack::lanhs", "group__norm.html#gac0c8d7a34e968fe5e06efc71f4916d8b", null ],
    [ "lapack::lanht", "group__norm.html#gaf53f509a2b1951bd40b2b3d9b20fa4ec", null ],
    [ "lapack::lansb", "group__norm.html#ga74ef528afc8cb577ff2e486ab705b09d", null ],
    [ "lapack::lansp", "group__norm.html#gaadac623350188380f9bd03bfd1109593", null ],
    [ "lapack::lanst", "group__norm.html#gae524dc328ee487cb74d8602c01218ff3", null ],
    [ "lapack::lansy", "group__norm.html#gad456ea028866036dc632a30678e3abd3", null ],
    [ "lapack::lantb", "group__norm.html#ga77d737995355be49b7ef9d7793d824df", null ],
    [ "lapack::lantp", "group__norm.html#gab5fff8010dbf23fe6eb1e570b2609194", null ],
    [ "lapack::lantr", "group__norm.html#ga31f0b6244f5e94d9d6991ede7c05d22c", null ]
];