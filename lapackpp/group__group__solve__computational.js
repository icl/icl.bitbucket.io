var group__group__solve__computational =
[
    [ "General matrix: LU", "group__gesv__computational.html", "group__gesv__computational" ],
    [ "General matrix: LU: banded", "group__gbsv__computational.html", "group__gbsv__computational" ],
    [ "General matrix: LU: tridiagonal", "group__gtsv__computational.html", "group__gtsv__computational" ],
    [ "Positive definite: Cholesky", "group__posv__computational.html", "group__posv__computational" ],
    [ "Positive definite: Cholesky: packed", "group__ppsv__computational.html", "group__ppsv__computational" ],
    [ "Positive definite: Cholesky: RFP", "group__pfsv__computational.html", null ],
    [ "Positive definite: Cholesky: banded", "group__pbsv__computational.html", "group__pbsv__computational" ],
    [ "Positive definite: Cholesky: tridiagonal", "group__ptsv__computational.html", "group__ptsv__computational" ],
    [ "Symmetric indefinite: Bunch-Kaufman", "group__sysv__computational.html", "group__sysv__computational" ],
    [ "Symmetric indefinite: Bunch-Kaufman: packed", "group__spsv__computational.html", null ],
    [ "Symmetric indefinite: Rook", "group__sysv__rk__computational.html", "group__sysv__rk__computational" ],
    [ "Symmetric indefinite: Aasen's", "group__sysv__aa__computational.html", "group__sysv__aa__computational" ],
    [ "Hermitian indefinite: Bunch-Kaufman", "group__hesv__computational.html", "group__hesv__computational" ],
    [ "Hermitian indefinite: Bunch-Kaufman: packed", "group__hpsv__computational.html", null ],
    [ "Hermitian indefinite: Rook", "group__hesv__rk__computational.html", "group__hesv__rk__computational" ],
    [ "Hermitian indefinite: Aasen's", "group__hesv__aa__computational.html", "group__hesv__aa__computational" ],
    [ "Triangular", "group__trsv__computational.html", null ],
    [ "Triangular: packed", "group__tpsv__computational.html", null ],
    [ "Triangular: RFP", "group__tfsv__computational.html", null ],
    [ "Triangular: banded", "group__tbsv__computational.html", null ]
];