var searchData=
[
  ['test_20matrix_20generation_0',['Test matrix generation',['../group__generate__matrix.html',1,'']]],
  ['test_20routines_1',['Test routines',['../group__group__test.html',1,'']]],
  ['triangular_2',['Triangular',['../group__trsv__computational.html',1,'']]],
  ['triangular_3a_20banded_3',['Triangular: banded',['../group__tbsv__computational.html',1,'']]],
  ['triangular_3a_20packed_4',['Triangular: packed',['../group__tpsv__computational.html',1,'']]],
  ['triangular_3a_20rfp_5',['Triangular: RFP',['../group__tfsv__computational.html',1,'']]]
];
