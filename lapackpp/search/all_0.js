var searchData=
[
  ['a_20_3d_20lq_20factorization_0',['A = LQ factorization',['../group__gelqf.html',1,'']]],
  ['a_20_3d_20lq_20factorization_2c_20triangle_2dpentagonal_20tiles_1',['A = LQ factorization, triangle-pentagonal tiles',['../group__tplqt.html',1,'']]],
  ['a_20_3d_20ql_20factorization_2',['A = QL factorization',['../group__geqlf.html',1,'']]],
  ['a_20_3d_20qr_20factorization_3',['A = QR factorization',['../group__geqrf.html',1,'']]],
  ['a_20_3d_20qr_20factorization_2c_20triangle_2dpentagonal_20tiles_4',['A = QR factorization, triangle-pentagonal tiles',['../group__tpqrt.html',1,'']]],
  ['a_20_3d_20rq_20factorization_5',['A = RQ factorization',['../group__gerqf.html',1,'']]],
  ['a_20_3d_20rz_20factorization_6',['A = RZ factorization',['../group__tzrzf.html',1,'']]],
  ['ap_20_3d_20qr_20factorization_20with_20pivoting_7',['AP = QR factorization with pivoting',['../group__geqpf.html',1,'']]],
  ['auxiliary_20routines_8',['Auxiliary routines',['../group__group__aux.html',1,'']]]
];
