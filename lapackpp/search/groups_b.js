var searchData=
[
  ['schur_20form_2c_20a_20_3d_20ztz_5eh_0',['Schur form, A = ZTZ^H',['../group__gees.html',1,'']]],
  ['singular_20value_20decomposition_20_28svd_29_1',['Singular Value Decomposition (SVD)',['../group__group__svd.html',1,'']]],
  ['standard_2c_20a_20_3d_20u_20sigma_20v_5eh_2',['Standard, A = U Sigma V^H',['../group__gesvd.html',1,'']]],
  ['standard_2c_20a_20_3d_20u_20sigma_20v_5eh_2c_20bidiagonal_3',['Standard, A = U Sigma V^H, bidiagonal',['../group__bdsvd.html',1,'']]],
  ['standard_2c_20av_20_3d_20v_20lambda_4',['Standard, AV = V Lambda',['../group__geev.html',1,'(Global Namespace)'],['../group__heev.html',1,'(Global Namespace)']]],
  ['standard_2c_20av_20_3d_20v_20lambda_3a_20banded_5',['Standard, AV = V Lambda: banded',['../group__hbev.html',1,'']]],
  ['standard_2c_20av_20_3d_20v_20lambda_3a_20packed_6',['Standard, AV = V Lambda: packed',['../group__hpev.html',1,'']]],
  ['standard_2c_20av_20_3d_20v_20lambda_3a_20tridiagonal_7',['Standard, AV = V Lambda: tridiagonal',['../group__htev.html',1,'']]],
  ['standard_2c_20ax_20_3d_20b_8',['Standard, AX = B',['../group__gels.html',1,'']]],
  ['symmetric_20indefinite_9',['Symmetric indefinite',['../group__sysv.html',1,'']]],
  ['symmetric_20indefinite_3a_20aasen_27s_10',['Symmetric indefinite: Aasen&apos;s',['../group__sysv__aa__computational.html',1,'']]],
  ['symmetric_20indefinite_3a_20bunch_2dkaufman_11',['Symmetric indefinite: Bunch-Kaufman',['../group__sysv__computational.html',1,'']]],
  ['symmetric_20indefinite_3a_20bunch_2dkaufman_3a_20packed_12',['Symmetric indefinite: Bunch-Kaufman: packed',['../group__spsv__computational.html',1,'']]],
  ['symmetric_20indefinite_3a_20packed_13',['Symmetric indefinite: packed',['../group__spsv.html',1,'']]],
  ['symmetric_20indefinite_3a_20rook_14',['Symmetric indefinite: Rook',['../group__sysv__rk__computational.html',1,'']]],
  ['symmetric_2fhermitian_20eigenvalues_15',['Symmetric/Hermitian eigenvalues',['../group__group__symmetric__eigen.html',1,'']]],
  ['symv_3a_20_20_20_20symmetric_20matrix_2dvector_20multiply_16',['symv:    Symmetric matrix-vector multiply',['../group__symv.html',1,'']]],
  ['syr_3a_20_20_20_20_20symmetric_20rank_201_20update_17',['syr:     Symmetric rank 1 update',['../group__syr.html',1,'']]]
];
