var searchData=
[
  ['general_20matrix_3a_20lu_0',['General matrix: LU',['../group__gesv.html',1,'(Global Namespace)'],['../group__gesv__computational.html',1,'(Global Namespace)']]],
  ['general_20matrix_3a_20lu_3a_20banded_1',['General matrix: LU: banded',['../group__gbsv.html',1,'(Global Namespace)'],['../group__gbsv__computational.html',1,'(Global Namespace)']]],
  ['general_20matrix_3a_20lu_3a_20tridiagonal_2',['General matrix: LU: tridiagonal',['../group__gtsv.html',1,'(Global Namespace)'],['../group__gtsv__computational.html',1,'(Global Namespace)']]],
  ['generalized_3',['Generalized',['../group__ggsvd.html',1,'']]],
  ['generalized_20qr_20factorization_4',['Generalized QR factorization',['../group__ggqrf.html',1,'']]],
  ['generalized_20rq_20factorization_5',['Generalized RQ factorization',['../group__ggrqf.html',1,'']]],
  ['generalized_20schur_20form_6',['Generalized Schur form',['../group__gges.html',1,'']]],
  ['generalized_20schur_20form_2c_20internal_7',['Generalized Schur form, internal',['../group__gges__internal.html',1,'']]],
  ['generalized_2c_20av_20_3d_20bv_20lambda_8',['Generalized, AV = BV Lambda',['../group__ggev.html',1,'']]],
  ['generalized_2c_20av_20_3d_20bv_20lambda_2c_20etc_2e_9',['Generalized, AV = BV Lambda, etc.',['../group__hygv.html',1,'']]],
  ['generalized_2c_20av_20_3d_20bv_20lambda_2c_20etc_2e_3a_20banded_10',['Generalized, AV = BV Lambda, etc.: banded',['../group__hbgv.html',1,'']]],
  ['generalized_2c_20av_20_3d_20bv_20lambda_2c_20etc_2e_3a_20packed_11',['Generalized, AV = BV Lambda, etc.: packed',['../group__hpgv.html',1,'']]]
];
