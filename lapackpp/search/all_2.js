var searchData=
[
  ['computational_20routines_0',['Computational routines',['../group__geev__computational.html',1,'(Global Namespace)'],['../group__gesvd__computational.html',1,'(Global Namespace)'],['../group__heev__computational.html',1,'(Global Namespace)']]],
  ['constrained_1',['Constrained',['../group__ggls.html',1,'']]],
  ['cosine_2dsine_20_28cs_29_20decomposition_2',['Cosine-Sine (CS) decomposition',['../group__bbcsd.html',1,'']]],
  ['cudatraits_3',['CudaTraits',['../classlapack_1_1_cuda_traits.html',1,'lapack']]]
];
