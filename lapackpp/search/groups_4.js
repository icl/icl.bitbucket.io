var searchData=
[
  ['hermitian_20indefinite_0',['Hermitian indefinite',['../group__hesv.html',1,'']]],
  ['hermitian_20indefinite_3a_20aasen_27s_1',['Hermitian indefinite: Aasen&apos;s',['../group__hesv__aa__computational.html',1,'']]],
  ['hermitian_20indefinite_3a_20bunch_2dkaufman_2',['Hermitian indefinite: Bunch-Kaufman',['../group__hesv__computational.html',1,'']]],
  ['hermitian_20indefinite_3a_20bunch_2dkaufman_3a_20packed_3',['Hermitian indefinite: Bunch-Kaufman: packed',['../group__hpsv__computational.html',1,'']]],
  ['hermitian_20indefinite_3a_20packed_4',['Hermitian indefinite: packed',['../group__hpsv.html',1,'']]],
  ['hermitian_20indefinite_3a_20rook_5',['Hermitian indefinite: Rook',['../group__hesv__rk__computational.html',1,'']]],
  ['householder_20reflectors_20and_20plane_20rotations_6',['Householder reflectors and plane rotations',['../group__unitary__computational.html',1,'']]]
];
