var searchData=
[
  ['orgbr_0',['orgbr',['../group__gesvd__computational.html#gadbae98deaac2febc9b70975d7e6e1a5a',1,'lapack']]],
  ['orglq_1',['orglq',['../group__gelqf.html#ga61b48cd6005572ca603ff483b7ec021c',1,'lapack']]],
  ['orgql_2',['orgql',['../group__geqlf.html#ga0db9430c08c7439e4753982a8f34607d',1,'lapack']]],
  ['orgqr_3',['orgqr',['../group__geqrf.html#ga7297b15cc969d1819141a6c4bddc9adb',1,'lapack']]],
  ['orgrq_4',['orgrq',['../group__gerqf.html#ga2525827f81474caaf142f8695ad88a6c',1,'lapack']]],
  ['orgtr_5',['orgtr',['../group__heev__computational.html#gaf5cf7de150664877d882de8d05669a24',1,'lapack']]],
  ['ormbr_6',['ormbr',['../group__gesvd__computational.html#ga813d09ecf94465821ec6d16604015ade',1,'lapack']]],
  ['ormlq_7',['ormlq',['../group__gelqf.html#gacda85c81c2dd665b1b212856b370606a',1,'lapack']]],
  ['ormql_8',['ormql',['../group__geqlf.html#ga3f2008bef6f33efa6d9a8802707ad6bc',1,'lapack']]],
  ['ormqr_9',['ormqr',['../group__geqrf.html#gaedf31f3203b14bb762a14b348b571b55',1,'lapack']]],
  ['ormrq_10',['ormrq',['../group__gerqf.html#gafe617933749f9d5b3f8212b33f69fda8',1,'lapack']]],
  ['ormtr_11',['ormtr',['../group__heev__computational.html#ga51a71c21a6756ca94f39179ab87f4667',1,'lapack']]],
  ['orthogonal_2funitary_20factorizations_20_28qr_2c_20etc_2e_29_12',['Orthogonal/unitary factorizations (QR, etc.)',['../group__group__unitary.html',1,'']]],
  ['other_20auxiliary_20routines_13',['Other auxiliary routines',['../group__auxiliary.html',1,'']]]
];
