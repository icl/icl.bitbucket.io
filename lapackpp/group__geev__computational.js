var group__geev__computational =
[
    [ "lapack::gebak", "group__geev__computational.html#ga0db37222a469cb2261b6f9a0ef5b0338", null ],
    [ "lapack::gebal", "group__geev__computational.html#ga4851162416dbd6b21ea6ba75e9fce5b9", null ],
    [ "lapack::gehrd", "group__geev__computational.html#ga53bfcba06b9a0e4a50bed842b6a08cd9", null ],
    [ "lapack::hseqr", "group__geev__computational.html#ga95c15ad7724cf31420ca561f135d62a4", null ],
    [ "lapack::trevc", "group__geev__computational.html#ga9e5f8f402a2de73c506cc9fde2aa796a", null ],
    [ "lapack::trevc3", "group__geev__computational.html#gaaae241dd4fc9618cfd78734314108127", null ],
    [ "lapack::unghr", "group__geev__computational.html#ga488c2be5251ad52421ccb747d47ba164", null ],
    [ "lapack::unmhr", "group__geev__computational.html#gaf0b95085ed60cab8d2ab0ca8c6dd89e6", null ]
];