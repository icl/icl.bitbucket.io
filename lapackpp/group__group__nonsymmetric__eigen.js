var group__group__nonsymmetric__eigen =
[
    [ "Standard, AV = V Lambda", "group__geev.html", "group__geev" ],
    [ "Generalized, AV = BV Lambda", "group__ggev.html", null ],
    [ "Schur form, A = ZTZ^H", "group__gees.html", null ],
    [ "Generalized Schur form", "group__gges.html", "group__gges" ],
    [ "Generalized Schur form, internal", "group__gges__internal.html", "group__gges__internal" ],
    [ "Computational routines", "group__geev__computational.html", "group__geev__computational" ]
];