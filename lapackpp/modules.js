var modules =
[
    [ "Linear solve, AX = B", "group__group__solve.html", "group__group__solve" ],
    [ "Linear solve: computational routines", "group__group__solve__computational.html", "group__group__solve__computational" ],
    [ "Least squares", "group__group__gels.html", "group__group__gels" ],
    [ "Orthogonal/unitary factorizations (QR, etc.)", "group__group__unitary.html", "group__group__unitary" ],
    [ "Symmetric/Hermitian eigenvalues", "group__group__symmetric__eigen.html", "group__group__symmetric__eigen" ],
    [ "Non-symmetric eigenvalues", "group__group__nonsymmetric__eigen.html", "group__group__nonsymmetric__eigen" ],
    [ "Singular Value Decomposition (SVD)", "group__group__svd.html", "group__group__svd" ],
    [ "Auxiliary routines", "group__group__aux.html", "group__group__aux" ],
    [ "BLAS extensions in LAPACK", "group__group__blas.html", "group__group__blas" ],
    [ "Test routines", "group__group__test.html", "group__group__test" ]
];