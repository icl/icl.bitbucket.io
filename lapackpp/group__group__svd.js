var group__group__svd =
[
    [ "Standard, A = U Sigma V^H", "group__gesvd.html", "group__gesvd" ],
    [ "Standard, A = U Sigma V^H, bidiagonal", "group__bdsvd.html", "group__bdsvd" ],
    [ "Generalized", "group__ggsvd.html", null ],
    [ "Computational routines", "group__gesvd__computational.html", "group__gesvd__computational" ]
];