var group__group__unitary =
[
    [ "A = QR factorization", "group__geqrf.html", "group__geqrf" ],
    [ "A = QR factorization, triangle-pentagonal tiles", "group__tpqrt.html", "group__tpqrt" ],
    [ "AP = QR factorization with pivoting", "group__geqpf.html", null ],
    [ "A = LQ factorization", "group__gelqf.html", "group__gelqf" ],
    [ "A = LQ factorization, triangle-pentagonal tiles", "group__tplqt.html", "group__tplqt" ],
    [ "A = QL factorization", "group__geqlf.html", "group__geqlf" ],
    [ "A = RQ factorization", "group__gerqf.html", "group__gerqf" ],
    [ "A = RZ factorization", "group__tzrzf.html", null ],
    [ "Generalized QR factorization", "group__ggqrf.html", null ],
    [ "Generalized RQ factorization", "group__ggrqf.html", null ],
    [ "Cosine-Sine (CS) decomposition", "group__bbcsd.html", "group__bbcsd" ],
    [ "Householder reflectors and plane rotations", "group__unitary__computational.html", "group__unitary__computational" ]
];