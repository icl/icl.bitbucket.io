var group__group__aux =
[
    [ "Initialize, copy, convert matrices", "group__initialize.html", "group__initialize" ],
    [ "Matrix norms", "group__norm.html", "group__norm" ],
    [ "Other auxiliary routines", "group__auxiliary.html", "group__auxiliary" ]
];