var group__gges__internal =
[
    [ "lapack::impl::tgexc", "group__gges__internal.html#ga3c793e50712ca21abb2c8b3028cef195", null ],
    [ "lapack::internal::tgexc", "group__gges__internal.html#ga4d9bea8ac83c8e86954f2909bd1ac005", null ],
    [ "lapack::internal::tgexc", "group__gges__internal.html#ga394048f4d64fbe2d39c2f6bec98513e5", null ],
    [ "lapack::internal::tgexc", "group__gges__internal.html#gac9d3d5a2e6e019557106ebbab11aea08", null ],
    [ "lapack::internal::tgexc", "group__gges__internal.html#ga1a182357173d816e553ae7d73f6c5a40", null ],
    [ "lapack::impl::tgsen", "group__gges__internal.html#gaaf44b47721bdbacdeeec901fa717e8a4", null ],
    [ "lapack::internal::tgsen", "group__gges__internal.html#ga1180835b2f4e712a7bbe7a1888d1cea1", null ],
    [ "lapack::internal::tgsen", "group__gges__internal.html#ga40b9b31c83794b656aa96b76fc52dd53", null ],
    [ "lapack::internal::tgsen", "group__gges__internal.html#ga895a35c926db034611908573a178e92d", null ],
    [ "lapack::internal::tgsen", "group__gges__internal.html#gaf74e6bec03e7b3ef319f0af922bfc7b4", null ]
];