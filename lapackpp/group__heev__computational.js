var group__heev__computational =
[
    [ "lapack::hetrd", "group__heev__computational.html#ga9c6a4125c583e3b10d1d25a79af6029f", null ],
    [ "lapack::hetrd_2stage", "group__heev__computational.html#ga8e5f48bb9bde08ec33bc519a924973ee", null ],
    [ "lapack::orgtr", "group__heev__computational.html#gaf5cf7de150664877d882de8d05669a24", null ],
    [ "lapack::ormtr", "group__heev__computational.html#ga51a71c21a6756ca94f39179ab87f4667", null ],
    [ "lapack::sturm", "group__heev__computational.html#ga5da508c96bf00aed407400cc1b1f86ef", null ],
    [ "lapack::sytrd", "group__heev__computational.html#ga4e42f12a321a815f638a0bd19a67d9ff", null ],
    [ "lapack::sytrd_2stage", "group__heev__computational.html#ga7a44f403f4ab486c8f953361ec66b601", null ],
    [ "lapack::ungtr", "group__heev__computational.html#gad95988973e84f06a7ea2bc8010e13e99", null ],
    [ "lapack::unmtr", "group__heev__computational.html#ga42410a6884c5f1d779fd178731e6fb7b", null ],
    [ "lapack::upmtr", "group__heev__computational.html#ga42bd03180f1aeb5e7c0f30114b3c4a90", null ]
];