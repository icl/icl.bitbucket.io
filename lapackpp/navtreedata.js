/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "LAPACK++", "index.html", [
    [ "LAPACK++ Installation Notes", "md__i_n_s_t_a_l_l.html", [
      [ "About", "index.html#autotoc_md12", null ],
      [ "Documentation", "index.html#autotoc_md13", null ],
      [ "Getting Help", "index.html#autotoc_md14", null ],
      [ "Resources", "index.html#autotoc_md15", null ],
      [ "Contributing", "index.html#autotoc_md16", null ],
      [ "Acknowledgments", "index.html#autotoc_md17", null ],
      [ "License", "index.html#autotoc_md18", null ],
      [ "Synopsis", "md__i_n_s_t_a_l_l.html#autotoc_md19", null ],
      [ "Environment variables (Makefile and CMake)", "md__i_n_s_t_a_l_l.html#autotoc_md20", null ],
      [ "Options (Makefile and CMake)", "md__i_n_s_t_a_l_l.html#autotoc_md21", null ],
      [ "Makefile Installation", "md__i_n_s_t_a_l_l.html#autotoc_md22", [
        [ "Options", "md__i_n_s_t_a_l_l.html#autotoc_md23", null ],
        [ "Vendor notes", "md__i_n_s_t_a_l_l.html#autotoc_md24", null ],
        [ "Manual configuration", "md__i_n_s_t_a_l_l.html#autotoc_md25", null ]
      ] ],
      [ "CMake Installation", "md__i_n_s_t_a_l_l.html#autotoc_md26", [
        [ "Options", "md__i_n_s_t_a_l_l.html#autotoc_md27", null ]
      ] ]
    ] ],
    [ "Routines", "modules.html", "modules" ],
    [ "Classes", "annotated.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"group__auxiliary.html",
"group__posv__computational.html#ga26fd58468db5ab3ed976c9523e1c3f82"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';