var group__group__solve =
[
    [ "General matrix: LU", "group__gesv.html", "group__gesv" ],
    [ "General matrix: LU: banded", "group__gbsv.html", "group__gbsv" ],
    [ "General matrix: LU: tridiagonal", "group__gtsv.html", "group__gtsv" ],
    [ "Positive definite: Cholesky", "group__posv.html", "group__posv" ],
    [ "Positive definite: Cholesky: packed", "group__ppsv.html", "group__ppsv" ],
    [ "Positive definite: Cholesky: banded", "group__pbsv.html", "group__pbsv" ],
    [ "Positive definite: Cholesky: tridiagonal", "group__ptsv.html", "group__ptsv" ],
    [ "Symmetric indefinite", "group__sysv.html", "group__sysv" ],
    [ "Symmetric indefinite: packed", "group__spsv.html", null ],
    [ "Hermitian indefinite", "group__hesv.html", "group__hesv" ],
    [ "Hermitian indefinite: packed", "group__hpsv.html", null ]
];