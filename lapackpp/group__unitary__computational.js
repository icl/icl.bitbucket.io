var group__unitary__computational =
[
    [ "lapack::larf", "group__unitary__computational.html#gac224f76046a58ee8935f7c4e1257ea25", null ],
    [ "lapack::larfb", "group__unitary__computational.html#gadcb385ab1ca7534d0bce8d288b5eeb31", null ],
    [ "lapack::larfg", "group__unitary__computational.html#gaa3a29f0c78fd16dcab74d79577981cb5", null ],
    [ "lapack::larfgp", "group__unitary__computational.html#ga7fc501ead471f8e152bd399dfc6a4bc7", null ],
    [ "lapack::larft", "group__unitary__computational.html#gaebefb3df6dc69d64682250c45ead05f9", null ],
    [ "lapack::larfx", "group__unitary__computational.html#ga2be394739e1ae01f6454b0d03ccf721c", null ],
    [ "lapack::larfy", "group__unitary__computational.html#gac283c53a463783250767f78b27da7899", null ]
];