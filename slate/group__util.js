var group__util =
[
    [ "Constructor functions", "group__func.html", "group__func" ],
    [ "slate::conj_transpose", "group__util.html#ga513d56790a1162b5bfe5b79c55961f0a", null ],
    [ "slate::internal::next_power2", "group__util.html#gaafce68a3bc61f2c074f55ffaf49e93b3", null ],
    [ "slate::transpose", "group__util.html#ga18db89517dc9f1c4b3611df200526a63", null ]
];