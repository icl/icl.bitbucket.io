var group__func =
[
    [ "slate::func::device_1d_grid", "group__func.html#ga102b443c2441e940903882ecbefd699c", null ],
    [ "slate::func::device_2d_grid", "group__func.html#ga9863ce3619c51bb33ff87181e8b9d89c", null ],
    [ "slate::func::is_2d_cyclic_grid", "group__func.html#gaa03670d0843a5713da6eceb449f7edad", null ],
    [ "slate::func::max_blocksize", "group__func.html#ga6095c7257d5a6a92640494428a501dce", null ],
    [ "slate::func::process_1d_grid", "group__func.html#gaab821f73f08817831774b05ffc1b5075", null ],
    [ "slate::func::process_2d_grid", "group__func.html#gad2b76e44763752854ee50b289a8f0f15", null ],
    [ "slate::func::transpose_grid", "group__func.html#ga9817d79bd21d1e25fa9225a1822ff211", null ],
    [ "slate::func::uniform_blocksize", "group__func.html#ga90cf54fd1de6fc5b91507df1e5c71403", null ]
];