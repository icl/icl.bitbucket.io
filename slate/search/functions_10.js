var searchData=
[
  ['rbt_5ffill_0',['rbt_fill',['../group__gesv__internal.html#ga69dafd4eab664cfd06717a79cdb2b4f6',1,'slate::internal']]],
  ['rbt_5fgenerate_1',['rbt_generate',['../group__gesv__internal.html#ga66a332eebe31b2feb914d16cd774c19f',1,'slate::internal']]],
  ['real_2',['real',['../namespaceslate_1_1device.html#aa7ab30c23a7dcfc173d5ef0731b0592c',1,'slate::device']]],
  ['recv_3',['recv',['../classslate_1_1_tile.html#a28e99f32f7ad33fad788d40ddc3402ab',1,'slate::Tile']]],
  ['redistribute_4',['redistribute',['../group__copy__internal.html#gaaa5ddd671534f06cc29b5cf9939b59ce',1,'slate']]],
  ['reduce_5finfo_5',['reduce_info',['../namespaceslate_1_1internal.html#aed89c1124b12e2ba73f7d17742e2ff8c',1,'slate::internal']]],
  ['release_6',['release',['../classslate_1_1_matrix_storage.html#aedb31779cd3a4d933d062c1ec611cd4c',1,'slate::MatrixStorage']]],
  ['releaselocalworkspace_7',['releaseLocalWorkspace',['../classslate_1_1_base_matrix.html#a32ece95658629db6d7d4727c17f21faa',1,'slate::BaseMatrix::releaseLocalWorkspace()'],['../classslate_1_1_base_matrix.html#a4b411fce1b2c48a0e3ea0e0f351ce5d0',1,'slate::BaseMatrix::releaseLocalWorkspace(std::set&lt; ij_tuple &gt; &amp;tile_set)']]],
  ['releaselocalworkspacetile_8',['releaseLocalWorkspaceTile',['../classslate_1_1_base_matrix.html#abd01ad08aaf80aa63f7aecf7398dbd92',1,'slate::BaseMatrix']]],
  ['releaseremoteworkspace_9',['releaseRemoteWorkspace',['../classslate_1_1_base_matrix.html#a8dd061258709307c69cfe4e7ab8dda8e',1,'slate::BaseMatrix::releaseRemoteWorkspace(int64_t receive_count=1)'],['../classslate_1_1_base_matrix.html#a129fa66b3bf903b667eded290c1ed455',1,'slate::BaseMatrix::releaseRemoteWorkspace(std::set&lt; ij_tuple &gt; &amp;tile_set, int64_t release_count=1)']]],
  ['releaseremoteworkspacetile_10',['releaseRemoteWorkspaceTile',['../classslate_1_1_base_matrix.html#a042e077724a82a195387f637be6bc336',1,'slate::BaseMatrix']]],
  ['releaseworkspace_11',['releaseWorkspace',['../classslate_1_1_base_matrix.html#a923bea803ea11a6c1eb61d0f1a34d9af',1,'slate::BaseMatrix::releaseWorkspace()'],['../classslate_1_1_matrix_storage.html#ac6b27792ae92d770b569a8c649b2cc4d',1,'slate::MatrixStorage::releaseWorkspace()']]],
  ['releaseworkspacebuffer_12',['releaseWorkspaceBuffer',['../classslate_1_1_matrix_storage.html#adedbc6c922cb3f51715823f84eb2675c',1,'slate::MatrixStorage']]],
  ['reservedeviceworkspace_13',['reserveDeviceWorkspace',['../classslate_1_1_base_band_matrix.html#ae7495461beab68fd848c4769c9093668',1,'slate::BaseBandMatrix::reserveDeviceWorkspace()'],['../classslate_1_1_base_trapezoid_matrix.html#ad1be5495087a4e63ffcae4402d6b758a',1,'slate::BaseTrapezoidMatrix::reserveDeviceWorkspace()'],['../classslate_1_1_matrix_storage.html#a857eb5e59f45f3403e5c52346a07f5a3',1,'slate::MatrixStorage::reserveDeviceWorkspace()'],['../classslate_1_1_matrix.html#a19d0fb9414e32ad6c9f7cdf08b8308df',1,'slate::Matrix::reserveDeviceWorkspace()']]],
  ['reservehostworkspace_14',['reserveHostWorkspace',['../classslate_1_1_base_trapezoid_matrix.html#a43f984d9d95e10cfd74851dffe78e4ad',1,'slate::BaseTrapezoidMatrix::reserveHostWorkspace()'],['../classslate_1_1_matrix_storage.html#a087fae57e058adb78122dff906d0c0b7',1,'slate::MatrixStorage::reserveHostWorkspace()'],['../classslate_1_1_matrix.html#a395ade56bb852f0d56623014be412ecc',1,'slate::Matrix::reserveHostWorkspace()']]],
  ['resize_15',['resize',['../classslate_1_1internal_1_1_dev_vector.html#aed5deda231f3ad5b54fb243a2f3e1a97',1,'slate::internal::DevVector']]],
  ['roundup_16',['roundup',['../namespaceslate.html#a4eb0f40ee87288a5d9a67fe02a60aa07',1,'slate::roundup()'],['../namespaceslate_1_1device.html#a3dce3ac00519c24f395eb17a101a7781',1,'slate::device::roundup()']]],
  ['row0_5foffset_17',['row0_offset',['../classslate_1_1_base_matrix.html#a7ebe04b7301b465cfe059e1cc0e18972',1,'slate::BaseMatrix']]],
  ['rowincrement_18',['rowIncrement',['../classslate_1_1_tile.html#ab81cea1015a640b9969dca07f873fcdd',1,'slate::Tile']]]
];
