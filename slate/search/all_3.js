var searchData=
[
  ['data_0',['data',['../classslate_1_1_tile.html#ae6d92597fb180e7409fa77cf52cc698e',1,'slate::Tile::data() const'],['../classslate_1_1_tile.html#a9a1eb20d7e7b201223ca6e5028034b77',1,'slate::Tile::data()'],['../classslate_1_1internal_1_1_dev_vector.html#ac1f30c0999614eefcbd8ad5038c90a1c',1,'slate::internal::DevVector::data()']]],
  ['dc_1',['DC',['../namespaceslate.html#a475f634fd1023b87c26d0601d1e162a9acf75e54791dd1f49f918345fdfe2430b',1,'slate::DC'],['../namespaceslate.html#a15936b7c45fd16acb7a07e5f153efd59acf75e54791dd1f49f918345fdfe2430b',1,'slate::DC']]],
  ['debug_2',['Debug',['../classslate_1_1_debug.html',1,'slate']]],
  ['deprecated_20list_3',['Deprecated List',['../deprecated.html',1,'']]],
  ['depth_4',['Depth',['../group__enum.html#ggac97a2c5045464e6949b9a65a059b196aa675056ad1441b6375b2c5abd48c27ef1',1,'slate']]],
  ['destroyqueues_5',['destroyQueues',['../classslate_1_1_matrix_storage.html#ad695b788382e0201d21ee05c9888b1fb',1,'slate::MatrixStorage']]],
  ['device_6',['device',['../classslate_1_1_tile.html#a4c5f3cce8983c90e578ca432341107cc',1,'slate::Tile']]],
  ['device_5f1d_5fgrid_7',['device_1d_grid',['../group__func.html#ga102b443c2441e940903882ecbefd699c',1,'slate::func']]],
  ['device_5f2d_5fgrid_8',['device_2d_grid',['../group__func.html#ga9863ce3619c51bb33ff87181e8b9d89c',1,'slate::func']]],
  ['device_5fregions_5fbuild_9',['device_regions_build',['../namespaceslate_1_1internal.html#a515f16c8eb5f1740aa5767ab1ac5fea5',1,'slate::internal::device_regions_build(std::array&lt; std::reference_wrapper&lt; BaseMatrix&lt; scalar_t &gt; &gt;, mat_count &gt; mats, std::array&lt; scalar_t **, mat_count &gt; mats_array_host, int64_t device, std::function&lt; void(int64_t, int64_t, int64_t)&gt; extra_setup, std::vector&lt; int64_t &gt; &amp;irange, std::vector&lt; int64_t &gt; &amp;jrange)'],['../namespaceslate_1_1internal.html#a47f59fb2383c9c06930e55e5d7bb920b',1,'slate::internal::device_regions_build(std::array&lt; std::reference_wrapper&lt; BaseMatrix&lt; scalar_t &gt; &gt;, mat_count &gt; mats, std::array&lt; scalar_t **, mat_count &gt; mats_array_host, int64_t device, std::function&lt; void(int64_t, int64_t, int64_t)&gt; extra_setup={})']]],
  ['device_5fregions_5fparams_10',['device_regions_params',['../structslate_1_1internal_1_1device__regions__params.html',1,'slate::internal']]],
  ['device_5fregions_5frange_11',['device_regions_range',['../namespaceslate_1_1internal.html#a817a1cd0fbbd4dd5c769c121e2785e12',1,'slate::internal']]],
  ['devices_12',['Devices',['../group__enum.html#ggaf84c73d5e2cf203effad4b6cb3f96fb8ac03ca67dda321195d74c951097f240c6',1,'slate']]],
  ['devvector_13',['DevVector',['../classslate_1_1internal_1_1_dev_vector.html#a53a3dec4543506d33b30cfd37bf2a444',1,'slate::internal::DevVector::DevVector()'],['../classslate_1_1internal_1_1_dev_vector.html#a64ef1ad1485ca9e0cdb3052ff0488e9c',1,'slate::internal::DevVector::DevVector(size_t in_size, int device, blas::Queue &amp;queue)'],['../classslate_1_1internal_1_1_dev_vector.html',1,'slate::internal::DevVector&lt; scalar_t &gt;']]],
  ['difflapackmatrices_14',['diffLapackMatrices',['../classslate_1_1_debug.html#a1cba992469bf03a0b6510b04c735897f',1,'slate::Debug']]],
  ['driver_15',['Driver',['../group__cond.html',1,'(Global Namespace)'],['../group__gbsv.html',1,'(Global Namespace)'],['../group__gesv.html',1,'(Global Namespace)'],['../group__heev.html',1,'(Global Namespace)'],['../group__hegv.html',1,'(Global Namespace)'],['../group__hesv.html',1,'(Global Namespace)'],['../group__norm.html',1,'(Global Namespace)'],['../group__pbsv.html',1,'(Global Namespace)'],['../group__posv.html',1,'(Global Namespace)'],['../group__svd.html',1,'(Global Namespace)']]]
];
