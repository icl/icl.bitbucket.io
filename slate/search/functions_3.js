var searchData=
[
  ['data_0',['data',['../classslate_1_1_tile.html#ae6d92597fb180e7409fa77cf52cc698e',1,'slate::Tile::data() const'],['../classslate_1_1_tile.html#a9a1eb20d7e7b201223ca6e5028034b77',1,'slate::Tile::data()'],['../classslate_1_1internal_1_1_dev_vector.html#ac1f30c0999614eefcbd8ad5038c90a1c',1,'slate::internal::DevVector::data()']]],
  ['destroyqueues_1',['destroyQueues',['../classslate_1_1_matrix_storage.html#ad695b788382e0201d21ee05c9888b1fb',1,'slate::MatrixStorage']]],
  ['device_2',['device',['../classslate_1_1_tile.html#a4c5f3cce8983c90e578ca432341107cc',1,'slate::Tile']]],
  ['device_5f1d_5fgrid_3',['device_1d_grid',['../group__func.html#ga102b443c2441e940903882ecbefd699c',1,'slate::func']]],
  ['device_5f2d_5fgrid_4',['device_2d_grid',['../group__func.html#ga9863ce3619c51bb33ff87181e8b9d89c',1,'slate::func']]],
  ['device_5fregions_5fbuild_5',['device_regions_build',['../namespaceslate_1_1internal.html#a515f16c8eb5f1740aa5767ab1ac5fea5',1,'slate::internal::device_regions_build(std::array&lt; std::reference_wrapper&lt; BaseMatrix&lt; scalar_t &gt; &gt;, mat_count &gt; mats, std::array&lt; scalar_t **, mat_count &gt; mats_array_host, int64_t device, std::function&lt; void(int64_t, int64_t, int64_t)&gt; extra_setup, std::vector&lt; int64_t &gt; &amp;irange, std::vector&lt; int64_t &gt; &amp;jrange)'],['../namespaceslate_1_1internal.html#a47f59fb2383c9c06930e55e5d7bb920b',1,'slate::internal::device_regions_build(std::array&lt; std::reference_wrapper&lt; BaseMatrix&lt; scalar_t &gt; &gt;, mat_count &gt; mats, std::array&lt; scalar_t **, mat_count &gt; mats_array_host, int64_t device, std::function&lt; void(int64_t, int64_t, int64_t)&gt; extra_setup={})']]],
  ['device_5fregions_5frange_6',['device_regions_range',['../namespaceslate_1_1internal.html#a817a1cd0fbbd4dd5c769c121e2785e12',1,'slate::internal']]],
  ['devvector_7',['DevVector',['../classslate_1_1internal_1_1_dev_vector.html#a53a3dec4543506d33b30cfd37bf2a444',1,'slate::internal::DevVector::DevVector()'],['../classslate_1_1internal_1_1_dev_vector.html#a64ef1ad1485ca9e0cdb3052ff0488e9c',1,'slate::internal::DevVector::DevVector(size_t in_size, int device, blas::Queue &amp;queue)']]],
  ['difflapackmatrices_8',['diffLapackMatrices',['../classslate_1_1_debug.html#a1cba992469bf03a0b6510b04c735897f',1,'slate::Debug']]]
];
