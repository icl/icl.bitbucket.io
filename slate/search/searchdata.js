var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvw~",
  1: "abdefhilmnost",
  2: "s",
  3: "abcdefghijklmnoprstuvw~",
  4: "ilmnostu",
  5: "v",
  6: "glmnot",
  7: "abcdghilmnopqrstuw",
  8: "ct",
  9: "acdeghilmopqstu",
  10: "cdsu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Modules",
  10: "Pages"
};

