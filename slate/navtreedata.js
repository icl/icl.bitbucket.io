/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "SLATE", "index.html", [
    [ "CHANGELOG", "md__c_h_a_n_g_e_l_o_g.html", null ],
    [ "SLATE Installation Notes", "md__i_n_s_t_a_l_l.html", null ],
    [ "Usage", "md_docs_2usage.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Routines", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
    [ "Class Members", "functions.html", [
      [ "All", "functions.html", "functions_dup" ],
      [ "Functions", "functions_func.html", "functions_func" ],
      [ "Variables", "functions_vars.html", null ],
      [ "Related Symbols", "functions_rela.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"classslate_1_1_band_matrix.html",
"classslate_1_1_lock_guard.html#a8e5d10bf87fa1be2b9886fa251a4b502",
"functions_e.html",
"group__group__blas3.html",
"group__swap__tile.html#gad6bdff76a67dc5caef79f40e9f05f73d",
"namespaceslate.html#aca16539a2a57b8f75e9f4cfbdee1f2c9a8c68ec62fed0facf71a10bb0d11d7126"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';