var group__blas2__internal =
[
    [ "gemv:   General matrix-vector multiply", "group__gemv__internal.html", "group__gemv__internal" ],
    [ "ger:    General matrix rank 1 update", "group__ger__internal.html", "group__ger__internal" ],
    [ "geru:   General matrix rank 1 update, unconjugated", "group__geru__internal.html", "group__geru__internal" ],
    [ "hemv:   Hermitian matrix-vector multiply", "group__hemv__internal.html", "group__hemv__internal" ],
    [ "her:    Hermitian rank 1 update", "group__her__internal.html", "group__her__internal" ],
    [ "her2:   Hermitian rank 2 update", "group__her2__internal.html", "group__her2__internal" ],
    [ "symv:   Symmetric matrix-vector multiply", "group__symv__internal.html", "group__symv__internal" ],
    [ "syr:    Symmetric rank 1 update", "group__syr__internal.html", "group__syr__internal" ],
    [ "syr2:   Symmetric rank 2 update", "group__syr2__internal.html", "group__syr2__internal" ],
    [ "trmv:   Triangular matrix-vector multiply", "group__trmv__internal.html", "group__trmv__internal" ],
    [ "trsv:   Triangular matrix-vector solve", "group__trsv__internal.html", "group__trsv__internal" ]
];