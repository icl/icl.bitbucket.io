/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "BLAS++", "index.html", [
    [ "BLAS++ Installation Notes", "md__i_n_s_t_a_l_l.html", [
      [ "About", "index.html#autotoc_md6", null ],
      [ "Documentation", "index.html#autotoc_md7", null ],
      [ "Getting Help", "index.html#autotoc_md8", null ],
      [ "Resources", "index.html#autotoc_md9", null ],
      [ "Contributing", "index.html#autotoc_md10", null ],
      [ "Acknowledgments", "index.html#autotoc_md11", null ],
      [ "License", "index.html#autotoc_md12", null ],
      [ "Synopsis", "md__i_n_s_t_a_l_l.html#autotoc_md13", null ],
      [ "Environment variables (Makefile and CMake)", "md__i_n_s_t_a_l_l.html#autotoc_md14", null ],
      [ "Options (Makefile and CMake)", "md__i_n_s_t_a_l_l.html#autotoc_md15", null ],
      [ "Makefile Installation", "md__i_n_s_t_a_l_l.html#autotoc_md16", [
        [ "Options", "md__i_n_s_t_a_l_l.html#autotoc_md17", null ],
        [ "Vendor notes", "md__i_n_s_t_a_l_l.html#autotoc_md18", null ],
        [ "Manual configuration", "md__i_n_s_t_a_l_l.html#autotoc_md19", null ]
      ] ],
      [ "CMake Installation", "md__i_n_s_t_a_l_l.html#autotoc_md20", [
        [ "Options", "md__i_n_s_t_a_l_l.html#autotoc_md21", null ],
        [ "Cross-compiling", "md__i_n_s_t_a_l_l.html#autotoc_md22", null ]
      ] ]
    ] ],
    [ "Routines", "modules.html", "modules" ],
    [ "Classes", "annotated.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"group__asum.html",
"group__herk__internal.html#gad3a8f610a40d9b97d41c7beb88b7a7aa",
"group__trsm__internal.html#ga75c60257b2db28db4bfdc1c2f008e578"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';