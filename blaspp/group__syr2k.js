var group__syr2k =
[
    [ "blas::syr2k", "group__syr2k.html#ga08a950735783b8b6c29397c87227d65f", null ],
    [ "blas::syr2k", "group__syr2k.html#ga550f40bca87f1693cb967f42872b85a8", null ],
    [ "blas::syr2k", "group__syr2k.html#gad2407723a0509678d1c4d642b3606f84", null ],
    [ "blas::syr2k", "group__syr2k.html#ga1669649ce8aa9c8a2addf3dca5738830", null ],
    [ "blas::syr2k", "group__syr2k.html#gafcd190cb76c362af1a92fb23052c2efa", null ],
    [ "blas::syr2k", "group__syr2k.html#ga86917b8eaba9fbbb0572891c7621afc5", null ],
    [ "blas::syr2k", "group__syr2k.html#gad854d2b958df932e8f79051586d08848", null ],
    [ "blas::syr2k", "group__syr2k.html#ga33a8f6d428e2e7a0004d4b9a498ae9e4", null ],
    [ "blas::syr2k", "group__syr2k.html#gab87f3cf5d780202d44dae2ccb8a004e9", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#gae70f3fa08fba653b6ccc733844142fe9", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#ga2d2e8759f92d93fcd18858f6a01343d1", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#ga265ad134207686686b9fa3756790b027", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#gaaf96c2ba6f4f64db75429959bbabc018", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#gaf384e3b6d4eca587e4a1df0646fcd37b", null ],
    [ "blas::batch::syr2k", "group__syr2k.html#gadc67fbad6389bf262677eaadc97e12f9", null ]
];