var group__gemm =
[
    [ "blas::gemm", "group__gemm.html#ga23673764d5b87724cda0d708b91dd28a", null ],
    [ "blas::gemm", "group__gemm.html#gaa7402b879e9763517832f5c24f19f6e9", null ],
    [ "blas::gemm", "group__gemm.html#ga07a67fa3afa138dd2a22e3a72b96d3c6", null ],
    [ "blas::gemm", "group__gemm.html#ga355ff7ccccc705ca817f2eddcaafd501", null ],
    [ "blas::gemm", "group__gemm.html#gaf8614b39ff1b1e1c6465fcd7bcdd42e3", null ],
    [ "blas::gemm", "group__gemm.html#ga90f65d380de56b929b8c207cad36afd0", null ],
    [ "blas::gemm", "group__gemm.html#gaa54f5d0434c681d57b4bc3c7ba487f3b", null ],
    [ "blas::gemm", "group__gemm.html#ga05f134eb3bb9c09e76abd032f640b579", null ],
    [ "blas::gemm", "group__gemm.html#ga5503626c95c1ab0b71398473c899742d", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga3090d56cc8b0b59cb799bf43b12fb603", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga4ea4e840f2fcba9114e50dcfe33d5895", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga8fd0b8f604d2eab558577c0615db9219", null ],
    [ "blas::batch::gemm", "group__gemm.html#gab01ab7d8758e80f3a51d0a5d39a2b5c8", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga220d902982a06a6a2cccb8df3315d8fc", null ],
    [ "blas::batch::gemm", "group__gemm.html#gacb7bd05dfb0824209e9023bdfd3a98cc", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga807f009eae93fc5c95ded15013e56662", null ],
    [ "blas::batch::gemm", "group__gemm.html#gacc834e024fe45f5133b39863bb6e9c4f", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga0f2333133876f5e53424993d92047099", null ],
    [ "blas::batch::gemm", "group__gemm.html#gab337db834e93d890c1f35b821a3e0965", null ],
    [ "blas::batch::gemm", "group__gemm.html#ga0c9a66fd3358bc682fbdfe64db3aff96", null ],
    [ "blas::batch::gemm", "group__gemm.html#gac4a420f847545f2495efe2792dfedb8b", null ]
];