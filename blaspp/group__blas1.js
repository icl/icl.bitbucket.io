var group__blas1 =
[
    [ "asum:  Vector 1 norm (sum)", "group__asum.html", "group__asum" ],
    [ "axpy:  Add vectors", "group__axpy.html", "group__axpy" ],
    [ "copy:  Copy vector", "group__copy.html", "group__copy" ],
    [ "dot:   Dot (inner) product", "group__dot.html", "group__dot" ],
    [ "dotu:  Dot (inner) product, unconjugated", "group__dotu.html", "group__dotu" ],
    [ "iamax: Find max element", "group__iamax.html", "group__iamax" ],
    [ "nrm2:  Vector 2 norm", "group__nrm2.html", "group__nrm2" ],
    [ "rot:   Apply Givens plane rotation", "group__rot.html", "group__rot" ],
    [ "rotg:  Generate Givens plane rotation", "group__rotg.html", "group__rotg" ],
    [ "rotm:  Apply modified (fast) Givens plane rotation", "group__rotm.html", "group__rotm" ],
    [ "rotmg: Generate modified (fast) Givens plane rotation", "group__rotmg.html", "group__rotmg" ],
    [ "scal:  Scale vector", "group__scal.html", "group__scal" ],
    [ "swap:  Swap vectors", "group__swap.html", "group__swap" ]
];