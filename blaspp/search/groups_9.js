var searchData=
[
  ['scal_3a_20_20_20scale_20vector_0',['scal:   Scale vector',['../group__scal__internal.html',1,'']]],
  ['scal_3a_20_20scale_20vector_1',['scal:  Scale vector',['../group__scal.html',1,'']]],
  ['swap_3a_20_20_20swap_20vectors_2',['swap:   Swap vectors',['../group__swap__internal.html',1,'']]],
  ['swap_3a_20_20swap_20vectors_3',['swap:  Swap vectors',['../group__swap.html',1,'']]],
  ['symm_3a_20_20_20symmetric_20matrix_20multiply_4',['symm:   Symmetric matrix multiply',['../group__symm__internal.html',1,'']]],
  ['symm_3a_20_20symmetric_20matrix_20multiply_5',['symm:  Symmetric matrix multiply',['../group__symm.html',1,'']]],
  ['symv_3a_20_20_20_20symmetric_20matrix_2dvector_20multiply_6',['symv:    Symmetric matrix-vector multiply',['../group__symv.html',1,'']]],
  ['symv_3a_20_20_20symmetric_20matrix_2dvector_20multiply_7',['symv:   Symmetric matrix-vector multiply',['../group__symv__internal.html',1,'']]],
  ['syr2_3a_20_20_20_20symmetric_20rank_202_20update_8',['syr2:    Symmetric rank 2 update',['../group__syr2.html',1,'']]],
  ['syr2_3a_20_20_20symmetric_20rank_202_20update_9',['syr2:   Symmetric rank 2 update',['../group__syr2__internal.html',1,'']]],
  ['syr2k_3a_20_20symmetric_20rank_202k_20update_10',['syr2k:  Symmetric rank 2k update',['../group__syr2k__internal.html',1,'']]],
  ['syr2k_3a_20symmetric_20rank_202k_20update_11',['syr2k: Symmetric rank 2k update',['../group__syr2k.html',1,'']]],
  ['syr_3a_20_20_20_20_20symmetric_20rank_201_20update_12',['syr:     Symmetric rank 1 update',['../group__syr.html',1,'']]],
  ['syr_3a_20_20_20_20symmetric_20rank_201_20update_13',['syr:    Symmetric rank 1 update',['../group__syr__internal.html',1,'']]],
  ['syrk_3a_20_20_20symmetric_20rank_20k_20update_14',['syrk:   Symmetric rank k update',['../group__syrk__internal.html',1,'']]],
  ['syrk_3a_20_20symmetric_20rank_20k_20update_15',['syrk:  Symmetric rank k update',['../group__syrk.html',1,'']]]
];
