var searchData=
[
  ['trmm_3a_20_20_20triangular_20matrix_20multiply_0',['trmm:   Triangular matrix multiply',['../group__trmm__internal.html',1,'']]],
  ['trmm_3a_20_20triangular_20matrix_20multiply_1',['trmm:  Triangular matrix multiply',['../group__trmm.html',1,'']]],
  ['trmv_3a_20_20_20_20_20_20_20triangular_20matrix_2dvector_20multiply_2',['trmv:       Triangular matrix-vector multiply',['../group__trmv.html',1,'']]],
  ['trmv_3a_20_20_20triangular_20matrix_2dvector_20multiply_3',['trmv:   Triangular matrix-vector multiply',['../group__trmv__internal.html',1,'']]],
  ['trsm_3a_20_20_20triangular_20solve_20matrix_4',['trsm:   Triangular solve matrix',['../group__trsm__internal.html',1,'']]],
  ['trsm_3a_20_20triangular_20solve_20matrix_5',['trsm:  Triangular solve matrix',['../group__trsm.html',1,'']]],
  ['trsv_3a_20_20_20_20_20_20_20triangular_20matrix_2dvector_20solve_6',['trsv:       Triangular matrix-vector solve',['../group__trsv.html',1,'']]],
  ['trsv_3a_20_20_20triangular_20matrix_2dvector_20solve_7',['trsv:   Triangular matrix-vector solve',['../group__trsv__internal.html',1,'']]]
];
