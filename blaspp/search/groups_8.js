var searchData=
[
  ['rot_3a_20_20_20_20apply_20givens_20plane_20rotation_0',['rot:    Apply Givens plane rotation',['../group__rot__internal.html',1,'']]],
  ['rot_3a_20_20_20apply_20givens_20plane_20rotation_1',['rot:   Apply Givens plane rotation',['../group__rot.html',1,'']]],
  ['rotg_3a_20_20_20generate_20givens_20plane_20rotation_2',['rotg:   Generate Givens plane rotation',['../group__rotg__internal.html',1,'']]],
  ['rotg_3a_20_20generate_20givens_20plane_20rotation_3',['rotg:  Generate Givens plane rotation',['../group__rotg.html',1,'']]],
  ['rotm_3a_20_20_20apply_20modified_20_28fast_29_20givens_20plane_20rotation_4',['rotm:   Apply modified (fast) Givens plane rotation',['../group__rotm__internal.html',1,'']]],
  ['rotm_3a_20_20apply_20modified_20_28fast_29_20givens_20plane_20rotation_5',['rotm:  Apply modified (fast) Givens plane rotation',['../group__rotm.html',1,'']]],
  ['rotmg_3a_20_20generate_20modified_20_28fast_29_20givens_20plane_20rotation_6',['rotmg:  Generate modified (fast) Givens plane rotation',['../group__rotmg__internal.html',1,'']]],
  ['rotmg_3a_20generate_20modified_20_28fast_29_20givens_20plane_20rotation_7',['rotmg: Generate modified (fast) Givens plane rotation',['../group__rotmg.html',1,'']]]
];
