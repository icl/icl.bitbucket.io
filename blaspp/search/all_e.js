var searchData=
[
  ['revolve_0',['revolve',['../classblas_1_1_queue.html#ae3a0c576fe05714bb3f3ab4f5d3f9a12',1,'blas::Queue']]],
  ['rot_1',['rot',['../group__rot.html#ga94bef871c50a7ec6c6bbcae33f7fd147',1,'blas::rot(int64_t n, TX *x, int64_t incx, TY *y, int64_t incy, blas::real_type&lt; TX, TY &gt; c, blas::scalar_type&lt; TX, TY &gt; s)'],['../group__rot.html#ga91f524e4c0870b801de26ed2dc7c2a19',1,'blas::rot(int64_t n, std::complex&lt; float &gt; *x, int64_t incx, std::complex&lt; float &gt; *y, int64_t incy, float c, float s)'],['../group__rot.html#ga417fcd5a61116e411b6c08691be37255',1,'blas::rot(int64_t n, std::complex&lt; double &gt; *x, int64_t incx, std::complex&lt; double &gt; *y, int64_t incy, double c, double s)'],['../group__rot.html#gaf49066119dce2f164bb7ce7834a555e2',1,'blas::rot(int64_t n, std::complex&lt; float &gt; *x, int64_t incx, std::complex&lt; float &gt; *y, int64_t incy, float c, std::complex&lt; float &gt; s)'],['../group__rot.html#ga84aa73535d9a3201ca73c6c28f2cc202',1,'blas::rot(int64_t n, std::complex&lt; double &gt; *x, int64_t incx, std::complex&lt; double &gt; *y, int64_t incy, double c, std::complex&lt; double &gt; s)']]],
  ['rot_3a_20_20_20_20apply_20givens_20plane_20rotation_2',['rot:    Apply Givens plane rotation',['../group__rot__internal.html',1,'']]],
  ['rot_3a_20_20_20apply_20givens_20plane_20rotation_3',['rot:   Apply Givens plane rotation',['../group__rot.html',1,'']]],
  ['rotg_4',['rotg',['../group__rotg.html#gad63ec429dcc16db2aed7891db1eeabc8',1,'blas::rotg(TA *a, TB *b, blas::real_type&lt; TA, TB &gt; *c, blas::real_type&lt; TA, TB &gt; *s)'],['../group__rotg.html#ga8dcd28a4996edaa23699d1c9efed5771',1,'blas::rotg(std::complex&lt; TA &gt; *a, std::complex&lt; TB &gt; *b, blas::real_type&lt; TA, TB &gt; *c, blas::complex_type&lt; TA, TB &gt; *s)']]],
  ['rotg_3a_20_20_20generate_20givens_20plane_20rotation_5',['rotg:   Generate Givens plane rotation',['../group__rotg__internal.html',1,'']]],
  ['rotg_3a_20_20generate_20givens_20plane_20rotation_6',['rotg:  Generate Givens plane rotation',['../group__rotg.html',1,'']]],
  ['rotm_7',['rotm',['../group__rotm.html#ga943f189058899c6f0baa3b9a73b4b554',1,'blas']]],
  ['rotm_3a_20_20_20apply_20modified_20_28fast_29_20givens_20plane_20rotation_8',['rotm:   Apply modified (fast) Givens plane rotation',['../group__rotm__internal.html',1,'']]],
  ['rotm_3a_20_20apply_20modified_20_28fast_29_20givens_20plane_20rotation_9',['rotm:  Apply modified (fast) Givens plane rotation',['../group__rotm.html',1,'']]],
  ['rotmg_10',['rotmg',['../group__rotmg.html#ga8335ebf701efc68844d31d85c0b025f2',1,'blas']]],
  ['rotmg_3a_20_20generate_20modified_20_28fast_29_20givens_20plane_20rotation_11',['rotmg:  Generate modified (fast) Givens plane rotation',['../group__rotmg__internal.html',1,'']]],
  ['rotmg_3a_20generate_20modified_20_28fast_29_20givens_20plane_20rotation_12',['rotmg: Generate modified (fast) Givens plane rotation',['../group__rotmg.html',1,'']]]
];
