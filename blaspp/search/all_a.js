var searchData=
[
  ['level_201_3a_20internal_20routines_2e_0',['Level 1: internal routines.',['../group__blas1__internal.html',1,'']]],
  ['level_201_3a_20vectors_20operations_2c_20o_28n_29_20work_1',['Level 1: vectors operations, O(n) work',['../group__blas1.html',1,'']]],
  ['level_202_3a_20internal_20routines_2e_2',['Level 2: internal routines.',['../group__blas2__internal.html',1,'']]],
  ['level_202_3a_20matrix_2dvector_20operations_2c_20o_28n_5e2_29_20work_3',['Level 2: matrix-vector operations, O(n^2) work',['../group__blas2.html',1,'']]],
  ['level_203_3a_20internal_20routines_2e_4',['Level 3: internal routines.',['../group__blas3__internal.html',1,'']]],
  ['level_203_3a_20matrix_2dmatrix_20operations_2c_20o_28n_5e3_29_20work_5',['Level 3: matrix-matrix operations, O(n^3) work',['../group__blas3.html',1,'']]]
];
