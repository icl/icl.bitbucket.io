var searchData=
[
  ['hemm_3a_20_20_20hermitian_20matrix_20multiply_0',['hemm:   Hermitian matrix multiply',['../group__hemm__internal.html',1,'']]],
  ['hemm_3a_20_20hermitian_20matrix_20multiply_1',['hemm:  Hermitian matrix multiply',['../group__hemm.html',1,'']]],
  ['hemv_3a_20_20_20_20hermitian_20matrix_2dvector_20multiply_2',['hemv:    Hermitian matrix-vector multiply',['../group__hemv.html',1,'']]],
  ['hemv_3a_20_20_20hermitian_20matrix_2dvector_20multiply_3',['hemv:   Hermitian matrix-vector multiply',['../group__hemv__internal.html',1,'']]],
  ['her2_3a_20_20_20_20hermitian_20rank_202_20update_4',['her2:    Hermitian rank 2 update',['../group__her2.html',1,'']]],
  ['her2_3a_20_20_20hermitian_20rank_202_20update_5',['her2:   Hermitian rank 2 update',['../group__her2__internal.html',1,'']]],
  ['her2k_3a_20_20hermitian_20rank_202k_20update_6',['her2k:  Hermitian rank 2k update',['../group__her2k__internal.html',1,'']]],
  ['her2k_3a_20hermitian_20rank_202k_20update_7',['her2k: Hermitian rank 2k update',['../group__her2k.html',1,'']]],
  ['her_3a_20_20_20_20_20hermitian_20rank_201_20update_8',['her:     Hermitian rank 1 update',['../group__her.html',1,'']]],
  ['her_3a_20_20_20_20hermitian_20rank_201_20update_9',['her:    Hermitian rank 1 update',['../group__her__internal.html',1,'']]],
  ['herk_3a_20_20_20hermitian_20rank_20k_20update_10',['herk:   Hermitian rank k update',['../group__herk__internal.html',1,'']]],
  ['herk_3a_20_20hermitian_20rank_20k_20update_11',['herk:  Hermitian rank k update',['../group__herk.html',1,'']]]
];
