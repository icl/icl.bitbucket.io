var searchData=
[
  ['gemm_3a_20_20_20general_20matrix_20multiply_0',['gemm:   General matrix multiply',['../group__gemm__internal.html',1,'']]],
  ['gemm_3a_20_20general_20matrix_20multiply_1',['gemm:  General matrix multiply',['../group__gemm.html',1,'']]],
  ['gemv_3a_20_20_20_20_20_20_20general_20matrix_2dvector_20multiply_2',['gemv:       General matrix-vector multiply',['../group__gemv.html',1,'']]],
  ['gemv_3a_20_20_20general_20matrix_2dvector_20multiply_3',['gemv:   General matrix-vector multiply',['../group__gemv__internal.html',1,'']]],
  ['ger_3a_20_20_20_20_20_20_20_20general_20matrix_20rank_201_20update_4',['ger:        General matrix rank 1 update',['../group__ger.html',1,'']]],
  ['ger_3a_20_20_20_20general_20matrix_20rank_201_20update_5',['ger:    General matrix rank 1 update',['../group__ger__internal.html',1,'']]],
  ['geru_3a_20_20_20_20_20_20_20general_20matrix_20rank_201_20update_2c_20unconjugated_6',['geru:       General matrix rank 1 update, unconjugated',['../group__geru.html',1,'']]],
  ['geru_3a_20_20_20general_20matrix_20rank_201_20update_2c_20unconjugated_7',['geru:   General matrix rank 1 update, unconjugated',['../group__geru__internal.html',1,'']]]
];
