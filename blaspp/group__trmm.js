var group__trmm =
[
    [ "blas::trmm", "group__trmm.html#gadd143755d234b34f475f4fe72fcc3667", null ],
    [ "blas::trmm", "group__trmm.html#ga7607e5e4d46ed7426a54910bf55cd171", null ],
    [ "blas::trmm", "group__trmm.html#ga67b25082c48a85c4b1d378b89effe5b7", null ],
    [ "blas::trmm", "group__trmm.html#ga1d7835571a1bce64d84e17b080c12efd", null ],
    [ "blas::trmm", "group__trmm.html#ga10494ba269f65af737386fbf4ab0810f", null ],
    [ "blas::trmm", "group__trmm.html#ga885baf7dd8fbe812e69bdb570abf8655", null ],
    [ "blas::trmm", "group__trmm.html#gaf96fc2780ecdbe6b975ef24f3859e37f", null ],
    [ "blas::trmm", "group__trmm.html#gae2531ce161a5d7a20558688357753b21", null ],
    [ "blas::trmm", "group__trmm.html#gafbf908b50c6f5aa9743e7f503dfebe8d", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga43a2943192a116faf3a7ebb1b0ee096e", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga3a0fd2dcd98bc1684f525347f3cf20cf", null ],
    [ "blas::batch::trmm", "group__trmm.html#gaa7bb9b2da4e03e7d568ace673fb8c146", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga91073bd285bf7f1eb99b3aff3f4f62c8", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga7457847d80427aea61c5491dfaef7c2a", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga399b7c21da76f5fb9bacf05e4193f961", null ],
    [ "blas::batch::trmm", "group__trmm.html#ga7f6dd9e0a2174cf38629deea472f9715", null ],
    [ "blas::batch::trmm", "group__trmm.html#gaed63d4ef6d020444b77823aea488c724", null ]
];