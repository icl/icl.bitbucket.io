var group__blas2 =
[
    [ "gemv:       General matrix-vector multiply", "group__gemv.html", "group__gemv" ],
    [ "ger:        General matrix rank 1 update", "group__ger.html", "group__ger" ],
    [ "geru:       General matrix rank 1 update, unconjugated", "group__geru.html", "group__geru" ],
    [ "hemv:    Hermitian matrix-vector multiply", "group__hemv.html", "group__hemv" ],
    [ "her:     Hermitian rank 1 update", "group__her.html", "group__her" ],
    [ "her2:    Hermitian rank 2 update", "group__her2.html", "group__her2" ],
    [ "symv:    Symmetric matrix-vector multiply", "group__symv.html", "group__symv" ],
    [ "syr:     Symmetric rank 1 update", "group__syr.html", "group__syr" ],
    [ "syr2:    Symmetric rank 2 update", "group__syr2.html", "group__syr2" ],
    [ "trmv:       Triangular matrix-vector multiply", "group__trmv.html", "group__trmv" ],
    [ "trsv:       Triangular matrix-vector solve", "group__trsv.html", "group__trsv" ]
];