var group__blas1__internal =
[
    [ "asum:   Vector 1 norm (sum)", "group__asum__internal.html", "group__asum__internal" ],
    [ "axpy:   Add vectors", "group__axpy__internal.html", "group__axpy__internal" ],
    [ "copy:   Copy vector", "group__copy__internal.html", "group__copy__internal" ],
    [ "dot:    Dot (inner) product", "group__dot__internal.html", "group__dot__internal" ],
    [ "dotu:   Dot (inner) product, unconjugated", "group__dotu__internal.html", "group__dotu__internal" ],
    [ "iamax:  Find max element", "group__iamax__internal.html", "group__iamax__internal" ],
    [ "nrm2:   Vector 2 norm", "group__nrm2__internal.html", "group__nrm2__internal" ],
    [ "rot:    Apply Givens plane rotation", "group__rot__internal.html", null ],
    [ "rotg:   Generate Givens plane rotation", "group__rotg__internal.html", null ],
    [ "rotm:   Apply modified (fast) Givens plane rotation", "group__rotm__internal.html", null ],
    [ "rotmg:  Generate modified (fast) Givens plane rotation", "group__rotmg__internal.html", null ],
    [ "scal:   Scale vector", "group__scal__internal.html", "group__scal__internal" ],
    [ "swap:   Swap vectors", "group__swap__internal.html", "group__swap__internal" ]
];