var modules =
[
    [ "Level 1: vectors operations, O(n) work", "group__blas1.html", "group__blas1" ],
    [ "Level 2: matrix-vector operations, O(n^2) work", "group__blas2.html", "group__blas2" ],
    [ "Level 3: matrix-matrix operations, O(n^3) work", "group__blas3.html", "group__blas3" ],
    [ "Level 1: internal routines.", "group__blas1__internal.html", "group__blas1__internal" ],
    [ "Level 2: internal routines.", "group__blas2__internal.html", "group__blas2__internal" ],
    [ "Level 3: internal routines.", "group__blas3__internal.html", "group__blas3__internal" ]
];